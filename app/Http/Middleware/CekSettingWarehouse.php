<?php

namespace warehouse\Http\Middleware;
use Closure;
use warehouse\Models\Companies;
use Illuminate\Support\Facades\Auth;

class CekSettingUserWarehouselist
{
    public function __construct(Companies $perusahaantbl)
    {
        $this->perusahaan = $perusahaantbl;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        // Perform action

            // this cek user roles
            $Authorized = Auth::User()->roles;
            foreach ($Authorized as $checkaccess) {
                # code...
                $results = $checkaccess->name;
            }
                if($results == 'super_users'){

                    $cek_company_by_owner = $this->perusahaan->where('owner_id', Auth::User()->id )->get();

                    if ($cek_company_by_owner->isEmpty()) {
                        # code...
                        return abort(403);
                    }

                }

        return $response;
    }

}