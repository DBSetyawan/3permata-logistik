<?php
 
namespace warehouse\Http\Middleware;
 
use Closure;
 
class validate
{
 /**
 * Handle an incoming request.
 *
 * @param \Illuminate\Http\Request $request
 * @param \Closure $next
 * @return mixed
 */
 public function handle($request, Closure $next)
 {
    $response = $next($request);

    return $response;
    return redirect()->route("role_branch_allowed.open", session()->get('id'))->withError("Maaf cabang yang anda pilih, tidak tersedia pada list cabang. \n [System Rollback]");

    }
}