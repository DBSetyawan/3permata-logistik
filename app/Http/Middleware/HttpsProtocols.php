<?php

namespace warehouse\Http\Middleware;

use Closure;

class HttpsProtocols
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $app_url = env('APP_URL');
// dd(substr($app_url, 0, 7) );
        if ( !$request->secure() && substr($app_url, 0, 7) === 'http://' ) {
            return redirect()->secure($request->getRequestUri());
        }
        return $next($request);
    }
}