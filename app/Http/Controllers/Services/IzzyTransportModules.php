<?php

 /**
 * [Author] Developer PT. 3 permata logistik. API Received webhooks from izzy transport.
 *
 * @param  \warehouse\Http\Controllers\Services\IzzytransportsHooks  $APIClient
 * @param  Interface @POD @POP @CREATE
 * @return \Illuminate\Http\Response $responseString
 */

namespace warehouse\Http\Controllers\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise\EachPromise;
use Auth;
use warehouse\Models\Transport_orders;
use warehouse\Http\Controllers\Controller;
use warehouse\Http\Controllers\Services\IzzytransportsHooks;
use warehouse\Models\Order_transport_history as TrackShipments;
use warehouse\Http\Controllers\Services\AccuratecloudInterface;
use warehouse\Http\Controllers\ManagementController as RESTAPIs;


class IzzyTransportModules extends Controller implements IzzytransportsHooks
{

    protected $openModulesAccurateCloud;
    protected $REST;
    public $datenow;
    protected $transport;

    public function __construct(
                                    Request $REST,
                                    AccuratecloudInterface $APInterfacecloud
                                )
    {

        $this->openModulesAccurateCloud = $APInterfacecloud;
        $this->rest = $REST;
        $this->transport = New Transport_orders;
        $this->datenow = date('d/m/Y');

    }

    public function ProcessingResponse(Transport_orders $tc, $response, $shipment_code){

        if($response == "CREATE"):

            return response()->json("shipment_created");

        endif;

        if($response == "POP"): //listeners event process dan membuat SO.
            
             $fetch_data = $tc->whereIn('order_id', [$shipment_code])->with(['customers','itemtransports.masteritemtc'])->get();

                foreach($fetch_data as $key => $thisDataTransports){

                    $order_id[] = $thisDataTransports->order_id;
                    $status_order__[] = $thisDataTransports->status_order_id;
                    $dataARRXCUSTOMER[] = $thisDataTransports->customers->itemID_accurate;
                    $dataARRXITEMTRANSPORT[] = $thisDataTransports->itemtransports->masteritemtc->itemID_accurate;
                    $SalesQuotationNumber[] = $thisDataTransports->recovery_SQ;
                    $dataHARGA[] = $thisDataTransports->harga;
                    $dataARRXQTITY[] = $thisDataTransports->quantity;
                    $dataARRXITEMTRANSPORTITEMUNIT[] = $thisDataTransports->itemtransports->unit;

                }

                $checkStatusShipment = $this->transport
                                                ->whereIn('status_order_id', [$status_order__[0]])
                                                    ->first();

                if($checkStatusShipment['status_order_id'] == 1):

                    $AccurateCloudSalesQuotation = $this->openModulesAccurateCloud
                        ->FuncOpenmoduleAccurateCloudSaveSalesQoutation(
                            'SQ.'.$order_id[0],
                            $dataARRXCUSTOMER[0],
                            $dataARRXITEMTRANSPORT[0],
                            $this->datenow,
                            $dataHARGA[0],
                            $dataARRXQTITY[0],
                            $dataARRXITEMTRANSPORTITEMUNIT[0]
                    );

                    $AccurateCloudSalesOrders = $this->openModulesAccurateCloud
                        ->FuncOpenmoduleAccurateCloudSaveSalesOrders(
                            'SO.'.$order_id[0],
                            $dataARRXCUSTOMER[0],
                            $dataARRXITEMTRANSPORT[0],
                            $this->datenow,
                            $AccurateCloudSalesQuotation->original,
                            $dataHARGA[0],
                            $dataARRXQTITY[0]
                    );


                    $tc->whereIn('order_id', [$order_id[0]])->update(
                        [
                            'salesQuotation_cloud' => substr($AccurateCloudSalesQuotation->original,0,2).'.'.$order_id[0],
                            'recovery_SQ' => $AccurateCloudSalesQuotation->original,
                            'status_order_id' => '8'
                        ]
                    );

                    $tc->whereIn('order_id', [$order_id[0]])->update(
                        [
                            'salesOrders_cloud' => substr($AccurateCloudSalesOrders->original,0,2).'.'.$order_id[0],
                            'recovery_SO' => $AccurateCloudSalesOrders->original,
                            'status_order_id' => '2'
                        ]
                    );
                              
                    /**
                     * end processing for next step async sales quotation
                     * 
                     */

                            $data_order[] = [
                                'user_id' => Auth::User()->id,
                                'order_id' => $order_id[0],
                                'status' => '8',
                                'datetime' => Carbon::now(),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];

                            $data_order[] = [
                                'user_id' => Auth::User()->id,
                                'order_id' => $order_id[0],
                                'status' => '2',
                                'datetime' => Carbon::now(),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];
    
                    TrackShipments::insert($data_order);

                    /**
                     * end processing for next step async sales orders
                     * 
                     */

                    return response()->json("shipment_PoP");

                else:

                        if($checkStatusShipment['status_order_id'] == 8):

                                $tc->whereIn('order_id', [$order_id[0]])->update(
                                    [
                                        'status_order_id' => '2'
                                    ]
                                );
                
                                        $data_order[] = [
                                            'user_id' => Auth::User()->id,
                                            'order_id' => $shipment_code,
                                            'status' => '2',
                                            'datetime' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                            'updated_at' => Carbon::now()
                                        ];
                            
                                    TrackShipments::insert($data_order);
                    
                                return response()->json("shipment_PoP");

                            else:

                                    $AccurateCloud = $this->openModulesAccurateCloud
                                        ->FuncOpenmoduleAccurateCloudSaveSalesOrders(
                                            'SO.'.$order_id[0],
                                            $dataARRXCUSTOMER[0],
                                            $dataARRXITEMTRANSPORT[0],
                                            $this->datenow,
                                            $SalesQuotationNumber[0],
                                            $dataHARGA[0],
                                            $dataARRXQTITY[0]
                                    );

                                    $tc->whereIn('order_id', [$order_id[0]])->update(
                                        [
                                            'salesOrders_cloud' => substr($AccurateCloud->original,0,2).'.'.$shipment_code,
                                            'recovery_SO' => $AccurateCloud->original,
                                            'status_order_id' => '2'
                                        ]
                                    );
                    
                                    $data_order[] = [
                                        'user_id' => Auth::User()->id,
                                        'order_id' => $shipment_code,
                                        'status' => '2',
                                        'datetime' => Carbon::now(),
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ];
                            
                                TrackShipments::insert($data_order);
                    
                            return response()->json("shipment_PoP");

                    endif;

                endif;

        endif;

        if($response == "POD"): //listeners event process dan membuat DO. format DO
            
            $fetch_data = $tc->whereIn('order_id', [$shipment_code])->with(['customers','itemtransports.masteritemtc'])->get();

                foreach($fetch_data as $key => $thisDataTransports){

                    $dataARRXCUSTOMER[] = $thisDataTransports->customers->itemID_accurate;
                    $dataARRXITEMTRANSPORT[] = $thisDataTransports->itemtransports->masteritemtc->itemID_accurate;
                    $SalesQuotationNumber[] = $thisDataTransports->recover_SQ;
                    $SalesOrdersNumber[] = $thisDataTransports->recovery_SO;
                    $dataARRXQTITYS[] = $thisDataTransports->quantity;
                    $dataHARGAS[] = $thisDataTransports->total_cost; 
                    $dataARRXITEMTRANSPORTITEMUNITS[] = $thisDataTransports->itemtransports->unit; 
                    $order_id[] = $thisDataTransports->order_id;

                }

                    $AccurateCloud = $this->openModulesAccurateCloud
                        ->FuncOpenmoduleAccurateCloudSaveDeliveryOrders(
                            'DO.'.$order_id[0],
                            $dataARRXCUSTOMER[0],
                            $this->datenow,
                            $dataARRXITEMTRANSPORT[0],
                            $SalesQuotationNumber[0],
                            $SalesOrdersNumber[0],
                            $dataARRXITEMTRANSPORTITEMUNITS[0],
                            $dataARRXQTITYS[0],
                            $dataHARGAS[0]
                    );
                    
                $tc->whereIn('order_id', [$order_id[0]])->update(
                    [
                        'deliveryOrders_cloud' => substr($AccurateCloud->original,0,2).'.'.$shipment_code,
                        'recovery_DO' => $AccurateCloud->original,
                        'status_order_id' => '4'
                    ]
                );

                    $data_order[] = [
                        'user_id' => Auth::User()->id,
                        'order_id' => $shipment_code,
                        'status' => '4',
                        'datetime' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
    
                TrackShipments::insert($data_order);

             return response()->json("shipment_POD");

        endif;

    }

}