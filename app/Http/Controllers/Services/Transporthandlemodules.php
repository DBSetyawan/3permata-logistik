<?php
namespace warehouse\Http\Controllers\Services;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use warehouse\Http\Controllers\Services\AccuratecloudInterface;

class Transporthandlemodules implements TransporthandleInterface
{

    protected $requests;
    protected $date;
    protected $access_token;
    protected $signatureSecretKey;
    protected $db_id;
    protected $session;
    
    public function __construct(Request $request)
    {
        $this->requests = $request; // from global request
        $this->date = gmdate('Y-m-d\TH:i:s\Z'); //this get date time allowed from format accurate cloud
        $this->access_token = "43039b2f-df0c-4c57-8f11-9dc1e6a84a74"; // WARNING: this access token getting from accurate cloud
        $this->signatureSecretKey = "8856ec43765b690c2193e894433c41eb"; // WARNIN:Signature from accurate cloud
        $this->db_id = "118845"; // this response to getting from accurate cloud
        $this->session = "094a4a65-d8b6-40f0-8a06-2456c2bc6123"; // this response getting session from accurate cloud
    }

    public function FuncOpenmoduleAccurateCloudAllMasterCustomerList($session, $fields, $date)
    {
        try 
            {

                $array_fetch_asort = [
                        'session' => $this->session,
                        'fields' => $fields,
                        '_ts' => $this->date
                    ]
                ;

                    ksort($array_fetch_asort);
                
                        $api = array_map('trim', $array_fetch_asort);
            
                $data = '';

                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);

                    $client = new Client();
                    
                        $response = $client->post(
                                'https://public.accurate.id/accurate/api/customer/list.do',
                                [
                                    'headers' => [
                                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                                    'Authorization' =>'bearer'.$this->access_token,
                                                    'Accept' => 'application/json'
                                                ],
                                    'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'fields' => $array_fetch_asort['fields'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                                ]
                            );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                return $response;

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

}
