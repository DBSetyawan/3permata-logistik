<?php

 /**
 * [Author] Developer PT. 3 permata logistik. API integrator to accurate cloud.
 *
 * @param  \warehouse\Http\Controllers\Services\AccuratecloudInterface $APIClient
 * @param  Interface @post | @get | @dispatch,put | @delete
 * @return \Illuminate\Http\ForceJsonResponse | $readStream === $response
 */

namespace warehouse\Http\Controllers\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise\EachPromise;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use warehouse\Http\Controllers\Services\AccuratecloudInterface;

class AccurateCloudmodules implements AccuratecloudInterface
{
 
    /**
     * @Application authroize accurate cloud 
     * refs : public.accurate.id/api
     */
    const sessionCloud = "2fb823c5-92ed-404b-862a-8598f112c2a1";
    const signatureSecretKey = "0843e6162e9893b12ba0c797c2f68a24";
    const access_token = "83f768cb-d700-4ba5-9c24-0fd2e09e627b";
    const DBINDEXACCURATE__ = "137848";

    /**
     * @method APIs webhooks signature hooks 3PS broadcasting
     * 
     */
    const PROTOCOL_WEBHOOK = "http://devsys.3permata.co.id/webhooks";

    /**
     * @method APIs Barang & jasa 
     * 
     */
        const API_ACCURATE_SAVE_ITEMBARANGJASA_URL = "https://public.accurate.id/accurate/api/item/save.do";
        const API_ACCURATE_DETAIL_ITEM_URL = "https://public.accurate.id/accurate/api/item/detail.do";
        const API_ACCURATE_LIST_BARANGJASA_URL = "https://public.accurate.id/accurate/api/item/list.do";
        const API_ACCURATE_SAVE_ITEM_URL = "https://public.accurate.id/accurate/api/item/save.do";
        const API_ACCURATE_ITEM_LIST_URL = "https://public.accurate.id/accurate/api/item/list.do";
        const API_ACCURATE_DETAIL_DATABASE_LIST_URL = "https://public.accurate.id/accurate/api/item/detail.do";

     /**
     * @method APIs Authorization & synchronize privilage db accurate cloud
     * 
     */
        const API_ACCURATE_AUTHORIZE_URL = "https://accurate.id/oauth/authorize";
        const API_ACCURATE_OPEN_DATABASE_SESSION_URL = "https://accurate.id/api/open-db.do";
        const API_ACCURATE_DATABASE_LIST_URL = "https://accurate.id/api/db-list.do";
        const API_ACCURATE_CHECK_DB_SESSION_URL = "https://accurate.id/api/db-check-session.do";

    /**
     * @method APIs Customers [ project ]
     * 
     */
    const API_ACCURATE_CUSTOMER_LIST_URL = "https://public.accurate.id/accurate/api/customer/list.do";
    const API_ACCURATE_SAVE_CUSTOMER_URL = "https://public.accurate.id/accurate/api/customer/save.do";
    const API_ACCURATE_UPDATE_CUSTOMER_URL = "https://public.accurate.id/accurate/api/customer/save.do";
    const API_ACCURATE_DETAIL_CUSTOMER_URL = "https://public.accurate.id/accurate/api/customer/detail.do";
    const API_ACCURATE_FIND_MASTER_CUSTOMER_ID_URL = "https://public.accurate.id/accurate/api/customer/detail.do";

    /**
     * @method APIs Sales Orders
     * 
     */
    const API_ACCURATE_DETAIL_SALES_ORDERS_URL = "https://public.accurate.id/accurate/api/sales-order/detail.do";
    const API_ACCURATE_SAVE_SALES_ORDERS_URL = "https://public.accurate.id/accurate/api/sales-order/save.do";
    const API_ACCURATE_SAVE_SALES_ORDER_URL = "https://public.accurate.id/accurate/api/sales-order/save.do";
    const API_ACCURATE_LIST_SALES_ORDERS_URL = "https://public.accurate.id/accurate/api/sales-order/list.do";

    /**
     * @method APIs Sales Quotation
     * 
     */
    const API_ACCURATE_SAVE_SALES_QUOTATION_URL = "https://public.accurate.id/accurate/api/sales-quotation/save.do";
    const API_ACCURATE_UPDATE_SALES_QUOTATION_URL = "https://public.accurate.id/accurate/api/sales-quotation/save.do";
    const API_ACCURATE_LIST_SALES_QUOTATION_URL = "https://public.accurate.id/accurate/api/sales-quotation/list.do";
    const API_ACCURATE_DETAIL_SALES_QUOTATION_URL = "https://public.accurate.id/accurate/api/sales-quotation/detail.do";

    /**
     * @method APIs vendor [ pemasok ]
     * 
     */
    const API_ACCURATE_SAVE_VENDOR_URL = "https://public.accurate.id/accurate/api/vendor/save.do";
    const API_ACCURATE_LIST_VENDOR_URL = "https://public.accurate.id/accurate/api/vendor/list.do";
    const API_ACCURATE_DETAIL_VENDOR_URL = "https://public.accurate.id/accurate/api/vendor/detail.do";
    const API_ACCURATE_UPDATE_VENDOR_URL = "https://public.accurate.id/accurate/api/vendor/save.do";

    /**
     * @method APIs delivery orders
     * 
     */
    const API_ACCURATE_SAVE_DELIVERY_ORDERS_URL = "https://public.accurate.id/accurate/api/delivery-order/save.do";

    /**
     * @method APIs Sales invoice
     * 
     */
    const API_ACCURATE_SAVE_SALES_INVOICE_URL = "https://public.accurate.id/accurate/api/sales-invoice/save.do";

     /**
     * @method APIs Sales receipts
     * 
     */
    const API_ACCURATE_SAVE_SALES_RECEIPT_URL = "https://public.accurate.id/accurate/api/sales-receipt/save.do";

    private $url;

    protected $requests;
    protected $date;
    protected $access_token;
    protected $signatureSecretKey;
    protected $db_id;
    protected $session;
    
    public function __construct(Request $request)
    {
        $this->requests = $request;
        $this->date = gmdate('Y-m-d\TH:i:s\Z');
        $this->signatureSecretKey = "0843e6162e9893b12ba0c797c2f68a24";
        $this->db_id = "131379";
        $this->session = "73632a01-b5d9-481e-80db-e2d554f64883";
    }

    public function FuncAuthorizedAccurateCloud($clientID, $responseType, $redirectURI, $scope){
        
        try
            {
                $client = new Client();
                $response = $client->post(
                    self::API_ACCURATE_AUTHORIZE_URL,
                        [
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                            [
                                'client_id' => $clientID,
                                'response_type' => $responseType,
                                'redirect_uri' => $redirectURI,
                                'scope' => $scope
                            ]
                        ]
                    );

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
        
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
    
        }

    }

    public function FuncOpenmoduleAccurateCloudItemList($fields, $itemType)
    {
        try 
            {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'fields' => $fields,
                        'itemType' => $itemType,
                        '_ts' => $this->date
                    ]
                ;

                    ksort($array_fetch_asort);
                
                        $api = array_map('trim', $array_fetch_asort);
            
                $data = '';

                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);

                    $client = new Client();
                    
                        $response = $client->post(
                                self::API_ACCURATE_ITEM_LIST_URL,
                                [
                                    'headers' => [
                                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                                    'Authorization' =>'bearer'.self::access_token,
                                                    'Accept' => 'application/json'
                                                ],
                                    'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'fields' => $array_fetch_asort['fields'],
                                        'itemType' => $array_fetch_asort['itemType'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                                ]
                            );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                return response()->json($jsonArray);

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

    public function FuncOpenmoduleAccurateCloudDblist($_ts)
    {
        try 
            {

                $array_fetch_asort = [
                        '_ts' => $_ts
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }

                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey  , true );
                $signature = base64_encode($hash);
                
                $client = new Client();

                $response = $client->post(
                        self::API_ACCURATE_DATABASE_LIST_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                [
                                    '_ts' => $array_fetch_asort['_ts'],
                                    'sign' => $signature
                                ]
                        ]
                    );

                $jsonArray = json_decode($response->getBody()->getContents(), true);

            return response()->json($jsonArray);

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {

                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }

    }

    public function FuncOpenmoduleAccurateCloudSession()
    {
        try {

            $array_fetch_asort = [
                        'id' => self::DBINDEXACCURATE__,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }

                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey  , true );
                $signature = base64_encode($hash);

                $client = new Client();

                $response = $client->post(
                        self::API_ACCURATE_OPEN_DATABASE_SESSION_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' =>
                            [
                                'id' => $array_fetch_asort['id'],
                                '_ts' => $array_fetch_asort['_ts'],
                                'sign' => $signature
                            ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                
                return $jsonArray["session"];

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
        
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudSaveSalesReceipt($bankno, $chequeAmount, $customerNo, $invoiceNo, $paymentAmount, $transDate){

        try 
            {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'bankNo' => $bankno,
                        'chequeAmount' => $chequeAmount,
                        'customerNo' => $customerNo,
                        'detailInvoice[0].invoiceNo' => $invoiceNo,
                        'detailInvoice[0].paymentAmount' => $paymentAmount,
                        'transDate' => $transDate,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_SALES_RECEIPT_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'bankNo' => $array_fetch_asort['bankNo'],
                                        'chequeAmount' => $array_fetch_asort['chequeAmount'],
                                        'customerNo' => $array_fetch_asort['customerNo'],
                                        'detailInvoice[0].invoiceNo' => $array_fetch_asort['detailInvoice[0].invoiceNo'],
                                        'detailInvoice[0].paymentAmount' => $array_fetch_asort['detailInvoice[0].paymentAmount'],
                                        'transDate' => $array_fetch_asort['transDate'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray["r"]["number"];

                return response()->json($data_array2);

            } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }

    }

    public function FuncOpenmoduleAccurateCloudSaveItemBarangjasa__($name, $itemType, $_ts, $kode_unik, $UnitName){

        try 
            {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'name' => $name,
                        'itemType' => $itemType,
                        '_ts' => $_ts,
                        'no' => $kode_unik,
                        'unit1Name' => $UnitName
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_ITEM_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'name' => $array_fetch_asort['name'],
                                        'itemType' => $array_fetch_asort['itemType'],
                                        'unit1Name' => $array_fetch_asort['unit1Name'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'no' => $array_fetch_asort['no'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    // parsing this id to db
                    $data_array2 = $jsonArray["r"]["no"];

                return response()->json($data_array2);

            } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }

    }
    
    public function FuncOpenmoduleAccurateCloudShowDetailDatabase($session, $id, $_ts){

        try {

            $array_fetch_asort = [
                    'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                    'id' => $id,
                    '_ts' => $_ts
                ]
            ;

            ksort($array_fetch_asort);
            
            $api = array_map('trim', $array_fetch_asort);
        
            $data = '';
            foreach ( $array_fetch_asort as $nama => $nilai ) {

                if ($nilai == '') {

                    continue;

                }
        
                    if ($data != '') {

                        $data .= '&';
                        
                    }

                $data .= rawurlencode($nama).'='.rawurlencode($nilai);

            }
            
            $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

            $signature = base64_encode($hash);
            $client = new Client();
            $response = $client->post(
                    self::API_ACCURATE_DETAIL_DATABASE_LIST_URL,
                    [
                        'headers' => [
                                        'Content-Type' => 'application/x-www-form-urlencoded',
                                        'Authorization' =>'bearer'.self::access_token,
                                        'Accept' => 'application/json'
                                    ],
                        'form_params' => 
                                [
                                    'session' => $array_fetch_asort['session'],
                                    'id' => $array_fetch_asort['id'],
                                    '_ts' => $array_fetch_asort['_ts'],
                                    'sign' => $signature
                                ]
                    ]
                );

                $jsonArray = json_decode($response->getBody()->getContents(), true);

            return response()->json($jsonArray);

        }
             catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }


    }

    public function FuncOpenmoduleAccurateCloudSaveCustomer(
                    $numbering, 
                    $name,
                    $detailName, 
                    $transDate, 
                    $email, 
                    $homePhone, 
                    $MobilePhone,
                    $website,
                    $Famiximili,
                    $alamatPenagihan,
                    $kotaPenagihan,
                    $billZipCode,
                    $billProvince,
                    $billCountry,
                    $whatsapp,
                    $npwpNo,
                    $CustomerTaxType
    ) 
        {

        try {

            $array_fetch_asort = [
                    'customerNo' => $numbering,
                    'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                    'name' => $name,
                    'transDate' => $transDate,
                    'email' => $email,
                    'homePhone' => $homePhone,
                    'mobilePhone' => $MobilePhone,
                    'fax' => $Famiximili,
                    'website' => $website,
                    'billStreet' => $alamatPenagihan,
                    'billCity' => $kotaPenagihan,
                    'billZipCode' => $billZipCode,
                    'billProvince' => $billProvince,
                    'billCountry' => $billCountry,
                    'npwpNo' => $npwpNo,
                    'customerTaxType' => $CustomerTaxType,
                    'detailContact[n].bbmPin' => $whatsapp,
                    '_ts' => $this->date
                ]
            ;

            ksort($array_fetch_asort);
            
            $api = array_map('trim', $array_fetch_asort);
        
            $data = '';
            foreach ( $array_fetch_asort as $nama => $nilai) {

                if ($nilai == '') {

                    continue;

                }
        
                    if ($data != '') {

                        $data .= '&';
                        
                    }

                $data .= rawurlencode($nama).'='.rawurlencode($nilai);

            }
            
            $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

            $signature = base64_encode($hash);
            $client = new Client();
            $response = $client->post(
                    self::API_ACCURATE_SAVE_CUSTOMER_URL,
                    [
                        'headers' => [
                                        'Content-Type' => 'application/x-www-form-urlencoded',
                                        'Authorization' =>'bearer'.self::access_token,
                                        'Accept' => 'application/json'
                                    ],
                        'form_params' => 
                                [
                                    'session' => $array_fetch_asort['session'],
                                    'customerNo' => $array_fetch_asort['customerNo'],
                                    'name' => $array_fetch_asort['name'],
                                    'transDate' => $array_fetch_asort['transDate'],
                                    'email' => $array_fetch_asort['email'],
                                    'homePhone' => $array_fetch_asort['homePhone'],
                                    'mobilePhone' => $array_fetch_asort['mobilePhone'],
                                    'fax' => $array_fetch_asort['fax'],
                                    'website' => $array_fetch_asort['website'],
                                    'billStreet' => $array_fetch_asort['billStreet'],
                                    'billCity' => $array_fetch_asort['billCity'],
                                    'billZipCode' => $array_fetch_asort['billZipCode'],
                                    'billProvince' => $array_fetch_asort['billProvince'],
                                    'billCountry' => $array_fetch_asort['billCountry'],
                                    'npwpNo' => $array_fetch_asort['npwpNo'],
                                    'customerTaxType' => $array_fetch_asort['customerTaxType'],
                                    'detailContact[n].bbmPin' => $array_fetch_asort['detailContact[n].bbmPin'],
                                    '_ts' => $array_fetch_asort['_ts'],
                                    'sign' => $signature
                                ]
                    ]
                );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                
                $responsecustomerid = $jsonArray["r"]["customerNo"];

            return response()->json($responsecustomerid);

        } catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }

    }

    public function FuncOpenmoduleAccurateCloudUpdateCustomer(
                    $id, 
                    // $numbering, 
                    $name
                    // $detailName, 
                    // $transDate, 
                    // $email, 
                    // $homePhone, 
                    // $MobilePhone,
                    // $website,
                    // $Famiximili,
                    // $alamatPenagihan,
                    // $kotaPenagihan,
                    // $billZipCode,
                    // $billProvince,
                    // $billCountry,
                    // $whatsapp,
                    // $npwpNo,
                    // $CustomerTaxType
            ) 
    {

            try {

            $array_fetch_asort = [
                    'id' => $id,
                    // 'customerNo' => $numbering,
                    'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                    'name' => $name,
                    // 'transDate' => $transDate,
                    // 'email' => $email,
                    // 'homePhone' => $homePhone,
                    // 'mobilePhone' => $MobilePhone,
                    // 'fax' => $Famiximili,
                    // 'website' => $website,
                    // 'billStreet' => $alamatPenagihan,
                    // 'billCity' => $kotaPenagihan,
                    // 'billZipCode' => $billZipCode,
                    // 'billProvince' => $billProvince,
                    // 'billCountry' => $billCountry,
                    // 'npwpNo' => $npwpNo,
                    // 'customerTaxType' => $CustomerTaxType,
                    // 'detailContact[n].bbmPin' => $whatsapp,
                    '_ts' => $this->date
                ]
            ;

            ksort($array_fetch_asort);

            $api = array_map('trim', $array_fetch_asort);

            $data = '';
            foreach ( $array_fetch_asort as $nama => $nilai) {

                if ($nilai == '') {

                    continue;

                }

                    if ($data != '') {

                        $data .= '&';
                        
                    }

                $data .= rawurlencode($nama).'='.rawurlencode($nilai);

            }

            $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

            $signature = base64_encode($hash);
            $client = new Client();
            $response = $client->post(
                    self::API_ACCURATE_UPDATE_CUSTOMER_URL,
                    [
                        'headers' => [
                                        'Content-Type' => 'application/x-www-form-urlencoded',
                                        'Authorization' =>'bearer'.self::access_token,
                                        'Accept' => 'application/json'
                                    ],
                        'form_params' => 
                                [
                                    'session' => $array_fetch_asort['session'],
                                    'id' => $array_fetch_asort['id'],
                                    // 'customerNo' => $array_fetch_asort['customerNo'],
                                    'name' => $array_fetch_asort['name'],
                                    // 'transDate' => $array_fetch_asort['transDate'],
                                    // 'email' => $array_fetch_asort['email'],
                                    // 'homePhone' => $array_fetch_asort['homePhone'],
                                    // 'mobilePhone' => $array_fetch_asort['mobilePhone'],
                                    // 'fax' => $array_fetch_asort['fax'],
                                    // 'website' => $array_fetch_asort['website'],
                                    // 'billStreet' => $array_fetch_asort['billStreet'],
                                    // 'billCity' => $array_fetch_asort['billCity'],
                                    // 'billZipCode' => $array_fetch_asort['billZipCode'],
                                    // 'billProvince' => $array_fetch_asort['billProvince'],
                                    // 'billCountry' => $array_fetch_asort['billCountry'],
                                    // 'npwpNo' => $array_fetch_asort['npwpNo'],
                                    // 'customerTaxType' => $array_fetch_asort['customerTaxType'],
                                    // 'detailContact[n].bbmPin' => $array_fetch_asort['detailContact[n].bbmPin'],
                                    '_ts' => $array_fetch_asort['_ts'],
                                    'sign' => $signature
                                ]
                    ]
                );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                
                $responsecustomerid = $jsonArray["r"]["customerNo"];

            return response()->json($responsecustomerid);

            } catch (\GuzzleHttp\Exception\ClientException $e) {

                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
            }

    }


    public function FuncOpenmoduleAccurateCloudUpdatePemasok(
                                                                $id, 
                                                                $name
                                                            ) 
    {

    try {

            $array_fetch_asort = [
                    'id' => $id,
                    'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                    'name' => $name,
                    '_ts' => $this->date
                ]
            ;

                    ksort($array_fetch_asort);

                    $api = array_map('trim', $array_fetch_asort);

                    $data = '';
                    foreach ( $array_fetch_asort as $nama => $nilai) {

                        if ($nilai == '') {

                            continue;

                        }

                            if ($data != '') {

                                $data .= '&';
                                
                            }

                        $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                    }

                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

            $signature = base64_encode($hash);
            $client = new Client();
            $response = $client->post(
                    self::API_ACCURATE_UPDATE_VENDOR_URL,
                    [
                        'headers' => [
                                        'Content-Type' => 'application/x-www-form-urlencoded',
                                        'Authorization' =>'bearer'.self::access_token,
                                        'Accept' => 'application/json'
                                    ],
                        'form_params' => 
                                [
                                    'session' => $array_fetch_asort['session'],
                                    'id' => $array_fetch_asort['id'],
                                    'name' => $array_fetch_asort['name'],
                                    '_ts' => $array_fetch_asort['_ts'],
                                    'sign' => $signature
                                ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                
                $responsecustomerid = $jsonArray["r"]["vendorNo"];

            return response()->json($responsecustomerid);

        } catch (\GuzzleHttp\Exception\ClientException $e) {

            return $e->getResponse()
                    ->getBody()
                    ->getContents();
                
        }

    }

    public function FuncOpenmoduleAccurateCloudSaveVendor(
                            $name, 
                            $detailName, 
                            $transDate, 
                            $email, 
                            $homePhone, 
                            $MobilePhone,
                            $website,
                            $Famiximili,
                            $alamatPenagihan,
                            $kotaPenagihan,
                            $billZipCode,
                            $billProvince,
                            $billCountry,
                            $whatsapp,
                            $npwpNo,
                            $CustomerTaxType
        ) 
            {

                try {

                    $array_fetch_asort = [
                            'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                            'name' => $name,
                            'transDate' => $transDate,
                            'email' => $email,
                            'homePhone' => $homePhone,
                            'mobilePhone' => $MobilePhone,
                            'fax' => $Famiximili,
                            'website' => $website,
                            'billStreet' => $alamatPenagihan,
                            'billCity' => $kotaPenagihan,
                            'billZipCode' => $billZipCode,
                            'billProvince' => $billProvince,
                            'billCountry' => $billCountry,
                            'npwpNo' => $npwpNo,
                            'customerTaxType' => $CustomerTaxType,
                            'detailContact[n].bbmPin' => $whatsapp,
                            '_ts' => $this->date
                        ]
                    ;

                    ksort($array_fetch_asort);

                    $api = array_map('trim', $array_fetch_asort);

                    $data = '';
                    foreach ( $array_fetch_asort as $nama => $nilai) {

                        if ($nilai == '') {

                            continue;

                        }

                            if ($data != '') {

                                $data .= '&';
                                
                            }

                        $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                    }

                    $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                    $signature = base64_encode($hash);
                    $client = new Client();
                    $response = $client->post(
                            self::API_ACCURATE_SAVE_VENDOR_URL,
                            [
                                'headers' => [
                                                'Content-Type' => 'application/x-www-form-urlencoded',
                                                'Authorization' =>'bearer'.self::access_token,
                                                'Accept' => 'application/json'
                                            ],
                                'form_params' => 
                                        [
                                            'session' => $array_fetch_asort['session'],
                                            'name' => $array_fetch_asort['name'],
                                            'transDate' => $array_fetch_asort['transDate'],
                                            'email' => $array_fetch_asort['email'],
                                            'homePhone' => $array_fetch_asort['homePhone'],
                                            'mobilePhone' => $array_fetch_asort['mobilePhone'],
                                            'fax' => $array_fetch_asort['fax'],
                                            'website' => $array_fetch_asort['website'],
                                            'billStreet' => $array_fetch_asort['billStreet'],
                                            'billCity' => $array_fetch_asort['billCity'],
                                            'billZipCode' => $array_fetch_asort['billZipCode'],
                                            'billProvince' => $array_fetch_asort['billProvince'],
                                            'billCountry' => $array_fetch_asort['billCountry'],
                                            'npwpNo' => $array_fetch_asort['npwpNo'],
                                            'customerTaxType' => $array_fetch_asort['customerTaxType'],
                                            'detailContact[n].bbmPin' => $array_fetch_asort['detailContact[n].bbmPin'],
                                            '_ts' => $array_fetch_asort['_ts'],
                                            'sign' => $signature
                                        ]
                            ]
                        );

                            $jsonArray = json_decode($response->getBody()->getContents(), true);
                        
                        $vendorno = $jsonArray["r"]["vendorNo"];

                    return response()->json($vendorno);

                } catch (\GuzzleHttp\Exception\ClientException $e) {

                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
            }

    }
    
    public function FuncOpenmoduleAccurateCloudSaveSalesQoutation($number, $customerno, $itemNo, $transDate, $unitPrice, $quantity, $ItemUnit){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'number' => $number,
                        'customerNo' => $customerno,
                        'detailItem[0].itemNo' => $itemNo,
                        'transDate' => $transDate,
                        'detailItem[0].unitPrice' => $unitPrice,
                        'detailItem[0].quantity' => $quantity,
                        'detailItem[0].itemUnitName' => $ItemUnit,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_SALES_QUOTATION_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'number' => $array_fetch_asort['number'],
                                        'detailItem[0].itemNo' => $array_fetch_asort['detailItem[0].itemNo'],
                                        'customerNo' => $array_fetch_asort['customerNo'],
                                        'transDate' => $array_fetch_asort['transDate'],
                                        'detailItem[0].unitPrice' => $array_fetch_asort['detailItem[0].unitPrice'],
                                        'detailItem[0].quantity' => $array_fetch_asort['detailItem[0].quantity'],
                                        'detailItem[0].itemUnitName' => $array_fetch_asort['detailItem[0].itemUnitName'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                    // dd($jsonArray);die;
                    $data_array2 = $jsonArray["r"]["number"];

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudListSalesQoutation($fields){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'keywords' => $fields,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_LIST_SALES_QUOTATION_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'keywords' => $array_fetch_asort['keywords'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray["d"][0]["id"];

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudListPemasok($fields){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'keywords' => $fields,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_LIST_VENDOR_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'keywords' => $array_fetch_asort['keywords'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray["d"][0]["id"];

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudListSalesOrders($fields){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'keywords' => $fields,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_LIST_SALES_ORDERS_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'keywords' => $array_fetch_asort['keywords'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray["d"][0]["id"];

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudUpdateSalesQoutation($qty, $id, $datailItemID, $detailPrice, $comments){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'detailItem[0].quantity' => $qty,
                        'detailItem[0].id' => $datailItemID,
                        'detailItem[0].unitPrice' => $detailPrice,
                        'detailItem[0].detailNotes' => $comments,
                        'id' => (String) $id,
                        '_ts' => $this->date
                    ]
                ;
                    // dd($array_fetch_asort);die;
                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_SALES_QUOTATION_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        'detailItem[0].quantity' => $array_fetch_asort['detailItem[0].quantity'],
                                        'detailItem[0].unitPrice' => $array_fetch_asort['detailItem[0].unitPrice'],
                                        'detailItem[0].id' => $array_fetch_asort['detailItem[0].id'],
                                        'detailItem[0].detailNotes' => $array_fetch_asort['detailItem[0].detailNotes'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudUpdateSalesOrders($detailNotes, $number, $id, $detailIDItem, $kuantitas, $price){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'poNumber' => $number,
                        'detailItem[0].detailNotes' => $detailNotes,
                        'detailItem[0].id' => $detailIDItem,
                        'detailItem[0].quantity' => $kuantitas,
                        'detailItem[0].unitPrice' => $price,
                        'id' => (String) $id,
                        '_ts' => $this->date
                    ]
                ;
                    // dd($array_fetch_asort);die;
                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_SALES_ORDERS_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        'poNumber' => $array_fetch_asort['poNumber'],
                                        'detailItem[0].id' => $array_fetch_asort['detailItem[0].id'],
                                        'detailItem[0].quantity' => $array_fetch_asort['detailItem[0].quantity'],
                                        'detailItem[0].detailNotes' => $array_fetch_asort['detailItem[0].detailNotes'],
                                        'detailItem[0].unitPrice' => $array_fetch_asort['detailItem[0].unitPrice'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudUpdateBarangJasa($id, $minimumQuantity, $Cost){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'detailOpenBalance[0].quantity' => $minimumQuantity,
                        'detailOpenBalance[0].unitCost' => $Cost,
                        'id' => (String) $id,
                        '_ts' => $this->date
                    ]
                ;
                    // dd($array_fetch_asort);die;
                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_ITEMBARANGJASA_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        'detailOpenBalance[0].quantity' => $array_fetch_asort['detailOpenBalance[0].quantity'],
                                        'detailOpenBalance[0].unitCost' => $array_fetch_asort['detailOpenBalance[0].unitCost'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudDetailSalesQoutation($id, $_ts){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'id' => $id,
                        '_ts' => $_ts
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_DETAIL_SALES_QUOTATION_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudDetailCustomers($id, $_ts){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'id' => $id,
                        '_ts' => $_ts
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_DETAIL_CUSTOMER_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudDetailPemasok($id){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'id' => $id,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_DETAIL_VENDOR_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudDetailSalesOrders($id, $_ts){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'id' => $id,
                        '_ts' => $_ts
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_DETAIL_SALES_ORDERS_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudDetailBarangJasa($id, $_ts){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'id' => $id,
                        '_ts' => $_ts
                    ]
                ;
                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_DETAIL_ITEM_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray;

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudSaveSalesOrders($number, $customerno, $itemNo, $transDate,$SalesQuotationNumber, $harga, $quantity){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'customerNo' => $customerno,
                        'detailItem[0].itemNo' => $itemNo,
                        'number' => $number,
                        'transDate' => $transDate,
                        'detailItem[0].salesQuotationNumber' => $SalesQuotationNumber,
                        'detailItem[0].unitPrice' => $harga,
                        'detailItem[0].quantity' => $quantity,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_SALES_ORDER_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'detailItem[0].itemNo' => $array_fetch_asort['detailItem[0].itemNo'],
                                        'customerNo' => $array_fetch_asort['customerNo'],
                                        'number' => $array_fetch_asort['number'],
                                        'transDate' => $array_fetch_asort['transDate'],
                                        'detailItem[0].salesQuotationNumber' => $array_fetch_asort['detailItem[0].salesQuotationNumber'],
                                        'detailItem[0].unitPrice' => $array_fetch_asort['detailItem[0].unitPrice'],
                                        'detailItem[0].quantity' => $array_fetch_asort['detailItem[0].quantity'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                    $data_array2 = $jsonArray["r"]["number"];
                    
                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudSaveDeliveryOrders($number, $customerno, $transDate ,$itemNo, $SalesQuotationNumber, $SalesOrdernumber, $itemUnit, $quantity, $price){

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'number' => $number,
                        'customerNo' => $customerno,
                        'detailItem[0].itemNo' => $itemNo,
                        'transDate' => $transDate,
                        'detailItem[0].salesQuotationNumber' => $SalesQuotationNumber,
                        'detailItem[0].salesOrderNumber' => $SalesOrdernumber,
                        'detailItem[0].itemUnitName' => $itemUnit,
                        'detailItem[0].quantity' => $quantity,
                        'detailItem[0].unitPrice' => $price,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_DELIVERY_ORDERS_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'detailItem[0].itemNo' => $array_fetch_asort['detailItem[0].itemNo'],
                                        'customerNo' => $array_fetch_asort['customerNo'],
                                        'number' => $array_fetch_asort['number'],
                                        'transDate' => $array_fetch_asort['transDate'],
                                        'detailItem[0].salesQuotationNumber' => $array_fetch_asort['detailItem[0].salesQuotationNumber'],
                                        'detailItem[0].salesOrderNumber' => $array_fetch_asort['detailItem[0].salesOrderNumber'],
                                        'detailItem[0].itemUnitName' => $array_fetch_asort['detailItem[0].itemUnitName'],
                                        'detailItem[0].quantity' => $array_fetch_asort['detailItem[0].quantity'],
                                        'detailItem[0].unitPrice' => $array_fetch_asort['detailItem[0].unitPrice'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                
                    $data_array2 = $jsonArray["r"]["number"];

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }

    public function FuncOpenmoduleAccurateCloudSaveSalesInvoice($customerno ,$itemNo, $OrderDownPaymentNumber, $reverseInvoice, $taxDate,
            $taxNumber, $transDate, $SO, $SQ, $DO, $price, $quantity, $itemUnitName
    )
     
     {

        try {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'customerNo' => $customerno,
                        'detailItem[0].itemNo' => $itemNo,
                        'orderDownPaymentNumber' => $OrderDownPaymentNumber,
                        'reverseInvoice' => $reverseInvoice,
                        'taxDate' => $taxDate,
                        'taxNumber' => $taxNumber,
                        'transDate' => $transDate,
                        'detailItem[0].salesOrderNumber' => $SO,
                        'detailItem[0].salesQuotationNumber' => $SQ,
                        'detailItem[0].deliveryOrderNumber' => $DO,
                        'detailItem[0].unitPrice' => $price,
                        'detailItem[0].quantity' => $quantity,
                        'detailItem[0].itemUnitName' => $itemUnitName,
                        '_ts' => $this->date
                    ]
                ;

                ksort($array_fetch_asort);
                
                $api = array_map('trim', $array_fetch_asort);
            
                $data = '';
                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true);

                $signature = base64_encode($hash);
                $client = new Client();
                $response = $client->post(
                        self::API_ACCURATE_SAVE_SALES_INVOICE_URL,
                        [
                            'headers' => [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],
                            'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'detailItem[0].itemNo' => $array_fetch_asort['detailItem[0].itemNo'],
                                        'detailItem[0].salesOrderNumber' => $array_fetch_asort['detailItem[0].salesOrderNumber'],
                                        'detailItem[0].salesQuotationNumber' => $array_fetch_asort['detailItem[0].salesQuotationNumber'],
                                        'detailItem[0].deliveryOrderNumber' => $array_fetch_asort['detailItem[0].deliveryOrderNumber'],
                                        'detailItem[0].unitPrice' => $array_fetch_asort['detailItem[0].unitPrice'],
                                        'detailItem[0].quantity' => $array_fetch_asort['detailItem[0].quantity'],
                                        'detailItem[0].itemUnitName' => $array_fetch_asort['detailItem[0].itemUnitName'],
                                        'customerNo' => $array_fetch_asort['customerNo'],
                                        'orderDownPaymentNumber' => $array_fetch_asort['orderDownPaymentNumber'],
                                        'reverseInvoice' => $array_fetch_asort['reverseInvoice'],
                                        'taxDate' => $array_fetch_asort['taxDate'],
                                        'taxNumber' => $array_fetch_asort['taxNumber'],
                                        'transDate' => $array_fetch_asort['transDate'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                        ]
                    );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);
                    
                    $data_array2 = $jsonArray["r"]["number"];

                return response()->json($data_array2);

            } 
                catch (\GuzzleHttp\Exception\ClientException $e) {
    
                    return $e->getResponse()
                            ->getBody()
                            ->getContents();
                        
        }

    }
    
    public function FuncOpenmoduleAccurateCloudAllMasterCustomerList($fields)
    {
        
        try 
            {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'keywords' => $fields,
                        '_ts' => $this->date
                    ]
                ;
                    
                    ksort($array_fetch_asort);
                
                        $api = array_map('trim', $array_fetch_asort);
            
                $data = '';

                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);

                    $client = new Client();
                    
                        $response = $client->post(
                                self::API_ACCURATE_CUSTOMER_LIST_URL,
                                [
                                    'headers' => [
                                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                                    'Authorization' =>'bearer'.self::access_token,
                                                    'Accept' => 'application/json'
                                                ],
                                    'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'keywords' => $array_fetch_asort['keywords'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                                ]
                            );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                return response()->json($jsonArray);

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

    public function FuncOpenmoduleAccurateCloudListBarangJasa($fields){
        
        try {
            
            $array_fetch_asort = [
                    'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                    'keywords' => $fields,
                    '_ts' => $this->date
                ]
            ;
                    ksort($array_fetch_asort);
                
                        $api = array_map('trim', $array_fetch_asort);
            
                $data = '';

                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);

                    $client = new Client();
                    $queue = \GuzzleHttp\Promise\queue();
                    
                            $promise = $client->postAsync(self::API_ACCURATE_LIST_BARANGJASA_URL,
                                [

                                    'headers' => 
                                        [
                                            'Content-Type' => 'application/x-www-form-urlencoded',
                                            'Authorization' =>'bearer'.self::access_token,
                                            'Accept' => 'application/json'
                                        ],

                                    'form_params' => 
                                        [
                                            'session' => $array_fetch_asort['session'],
                                            'keywords' => $array_fetch_asort['keywords'],
                                            '_ts' => $array_fetch_asort['_ts'],
                                            'sign' => $signature
                                        ]
                                        
                                ])->then(
                                    function (ResponseInterface $res){

                                        $response = json_decode($res->getBody()->getContents());
                                
                                        return response()->json($response);
                                      
                                },
                                    function (RequestException $e) {

                                        $response = [];
                                        $response[] = $e->getResponse()
                                                        ->getBody()
                                                        ->getContents();
                                    
                                        return response()->json($response);

                                }
                            );

                            $queue->run();
                            
                            $response = $promise->wait();

                return $response;

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

    public function FuncOpenmoduleAccurateCloudfindMasterCustomerID($session, $id, $_ts)
    {
        
        try 
            {

                $array_fetch_asort = [
                        'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                        'id' => $id,
                        '_ts' => $_ts
                    ]
                ;
                    
                    ksort($array_fetch_asort);
                
                        $api = array_map('trim', $array_fetch_asort);
            
                $data = '';

                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, $this->signatureSecretKey , true );

                $signature = base64_encode($hash);

                    $client = new Client();
                    
                        $response = $client->post(
                                self::API_ACCURATE_FIND_MASTER_CUSTOMER_ID_URL,
                                [
                                    'headers' => [
                                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                                    'Authorization' =>'bearer'.self::access_token,
                                                    'Accept' => 'application/json'
                                                ],
                                    'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        'id' => $array_fetch_asort['id'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                                ]
                            );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                return response()->json($jsonArray);

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

    public function FuncAlwaysOnSessionAccurateCluod(){

        return self::LiveAlwaysSessionAccurateCloud();

    }

    protected function LiveAlwaysSessionAccurateCloud(): String
    {
        
            if($this->funcModulesSessionAlwaysOn() == true):

                return self::sessionCloud;

                else:

                    return $this->FuncOpenmoduleAccurateCloudSession();

            endif;

    }

    public function FuncModulesSessionAlwaysOn()
    {
        
        try 
            {

                $array_fetch_asort = [
                        'session' => self::sessionCloud,
                        '_ts' => $this->date
                    ]
                ;
                    
                    ksort($array_fetch_asort);
                
                        $api = array_map('trim', $array_fetch_asort);
            
                $data = '';

                foreach ( $array_fetch_asort as $nama => $nilai ) {

                    if ($nilai == '') {

                        continue;

                    }
            
                        if ($data != '') {

                            $data .= '&';
                            
                        }

                    $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                }
                
                $hash = hash_hmac('sha256', $data, self::signatureSecretKey , true );

                $signature = base64_encode($hash);

                    $client = new Client();
                    
                        $response = $client->post(
                                self::API_ACCURATE_CHECK_DB_SESSION_URL,
                                [
                                    'headers' => [
                                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                                    'Authorization' =>'bearer'.self::access_token,
                                                    'Accept' => 'application/json'
                                                ],
                                    'form_params' => 
                                    [
                                        'session' => $array_fetch_asort['session'],
                                        '_ts' => $array_fetch_asort['_ts'],
                                        'sign' => $signature
                                    ]
                                ]
                            );

                    $jsonArray = json_decode($response->getBody()->getContents(), true);

                return $jsonArray["d"];


        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

    public function FuncModuleReceivedWebhook(Request $req)
    {
        
        try 
            {
                // $array_fetch_asort = [
                //         'id' => self::DBINDEXACCURATE__,
                //         'session' => $this->FuncAlwaysOnSessionAccurateCluod(),
                //         '_ts' => $this->date
                //     ]
                // ;
                //     ksort($array_fetch_asort);
                
                //         $api = array_map('trim', $array_fetch_asort);
            
                // $data = '';

                // foreach ( $array_fetch_asort as $nama => $nilai ) {

                //     if ($nilai == '') {

                //         continue;

                //     }
            
                //         if ($data != '') {

                //             $data .= '&';
                            
                //         }

                //     $data .= rawurlencode($nama).'='.rawurlencode($nilai);

                // }
                
                // $hash = hash_hmac('sha256', $data, self::signatureSecretKey , true );

                // $signature = base64_encode($hash);

                        
                    // $jsonArray = json_decode($client->getBody()->getContents(), true);
                    // $json = $promise;

                    // return response()->json($json);
                    // $client = new Client();
                    
                    // $request_body = file_get_contents('php://input');
                    // $json = json_decode($request_body, 1);
                    //  $headers = $this->requests->headers->all();
                    // return response()->json($request_body);


                //     if($json = json_decode(file_get_contents("php://input"), true)) {
                //         // print_r($json);
                //         $data = $json;
                //     } else {
                //         // print_r($req);
                //         $data = $req;
                //     }
                   
                // //    echo "Saving data ...\n";
                //    $url = 'http://devsys.3permata.co.id/webhooks';
                   
                //    $meta = ["received" => time(),
                //        "status" => "new",
                //        "agent" => $req->header('User-Agent')];
                   
                //    $options = ["http" => [
                //        "method" => "POST",
                //        "header" => ["Content-Type: application/json"],
                //        "content" => json_encode(["data" => $data, "meta" => $meta])]
                //        ];
                   
                //    $context = stream_context_create($options);
                //   $response = file_get_contents($url, false, $context);
                //   dd($req->all());

                    // $client->postAsync('http://webhook.site/961a995e-1ca6-464d-85e6-f9930bf402f5')->then(
                      
                    // );
                    // $promise1 = $client->request('POST', 'http://webhook.site/961a995e-1ca6-464d-85e6-f9930bf402f5', [
                    //     'curl' => [
                    //         CURLOPT_INTERFACE => '103.120.233.10'
                    //     ]
                    // ]);
                    
                  
                    // $response = $client->request('POST', $url,  ['headers' => [
                    //     'Content-Type' => 'application/json',
                    //     'x-forwarded-for' =>'103.120.233.10',
                    //     'user-agent' => 'Java/1.8.0_222',
                    //     'Accept' => 'text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2'
                    // ]]);
                    // curl_init($url);
                    // curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept:application/json, Content-Type:application/json']);
                    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    // $statusCode = $response->getStatusCode();
                    // $content = $response->getBody();
                    //    return response()->json($response);
                    // $jsonArray = $client->post('http://webhook.site/961a995e-1ca6-464d-85e6-f9930bf402f5')->getBody();
                    // $promise1->then(function ($response) {
                    //     echo 'Got a response! ' . $response->getStatusCode();
                    // });
                    // $promise1->wait();
                    // $results = Promise\settle($promises)->wait();
                    // return response()->json($results);
                //     $promise = $client->postAsync('webhook');
                //     $promises = (function () use($client) {
                //         yield $client->postAsync('http://devsys.3permata.co.id/webhook');		
                //     })();
                //     $eachPromise = new EachPromise($promises, [
                //       'fulfilled' => function (Response $response) {
                //         if ($response->getStatusCode() == 200) {
                //           $user = json_decode($response->getBody(), true);
                //             return response()->json($user);

                //           }
                //         },
                //       'rejected' => function ($reason) {
                //         return response()->json(['dasdasd'=>$reason]);


                //       }
                //     ]);

                //    $sdas = $eachPromise->promise()->wait();
                    // echo $jsonArray;

        } 
            catch (\GuzzleHttp\Exception\ClientException $e) {
    
                return $e->getResponse()
                        ->getBody()
                        ->getContents();
                    
        }
        
    }

}