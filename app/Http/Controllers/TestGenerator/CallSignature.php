<?php

namespace warehouse\Http\Controllers\TestGenerator;

use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Psr7\Response;
use Cache;
use GuzzleHttp\Promise\Promise;

Trait CallSignature
{

    public function test()
    {
       
        $users = ['one', 'two', 'three'];
        $cache = new Cache();
        $promises = (function () use ($users, $cache) {
        foreach ($users as $user) {
            
            if ($cache->hasItem('cache_user_' . $user)) {
            $profile = $cache->getItem($user)->get();
            yield new FulfilledPromise($profile);
            continue;
        }
            yield $this->getAsync($url. $user)
            ->then(function (Response $response) use ($cache, $user) {
                
                $profile = json_decode($response->getBody(), true);
                $cache->put('cache_user_' . $user, $profile, $expiry = 300);
                return $profile;
            });
        }
        })();
            $eachPromise = new EachPromise($promises, [
            'concurrency' => 4,
            'fulfilled' => function ($profile) {
                },
            'rejected' => function ($reason) {
            }
        ]);

        $eachPromise->promise()->wait();

    }

    public function GeneratorFetchJustTime(){

        $signatureSecret = "0843e6162e9893b12ba0c797c2f68a24";
        $date = gmdate('Y-m-d\TH:i:s\Z');

        $parameter['_ts'] = $date;
        $parameter['id'] = "150";
        $parameter['name'] = "testing response";
        $parameter['session'] = "667365d7-9916-4274-93c7-f58dd5c1fa77";
            ksort($parameter);
            
            $parameter = array_map('trim', $parameter);
        
            $data = '';
            foreach ( $parameter as $nama => $nilai ) {

                if ($nilai == '') {

                    continue;

                }
        
                    if ($data != '') {

                        $data .= '&';
                        
                    }

                $data .= rawurlencode($nama).'='.rawurlencode($nilai);

            }
        

            $hash = hash_hmac('sha256', $data, $signatureSecret, true);

            $signature = base64_encode($hash);
            
       return response()->json(['sign' => $signature, '_ts' => $date]);
         
    }  

    public function BinaryEncoderDecoder()
    {
        if(isset($request->encode)) {

            $words = (isset($request->encode) ? $request->encode : null) ;
            $encoded = "";

            $len = strlen($words);
            $i=0;

            for($i=0;$i<$len;$i++)
            {
                $itg = ord($words[$i]);  
                $bin = decbin($itg); 
                $new = str_pad($bin, 8, "0", STR_PAD_LEFT); 
                $encoded .= $new; 
            }
                
                echo "<center>";
                echo "<h3>Result : </h3>";
                echo "<textarea rows='15' cols='50' name='result'>";
                echo "$encoded";
                echo "</textarea>";
                echo "</center>";
                
            } 
                else if(isset($request->decode))
                {
                    
                    $bin = (isset($request->decode) ? $request->decode : null) ;
                    $decoded = "";
                    $arr = str_split($bin, 8);
                    $size = count($arr);
                    $i=0;
                    for($i=0;$i<$size;$i++)
                    {
                        $dec = bindec($arr[$i]); 
                        $char = chr($dec); 
                        $decoded .= $char;
                    }

                    echo "<center>";
                    echo "<h3>Result : </h3>";
                    echo "<textarea rows='15' cols='50' name='result'>";
                    echo "$decoded";
                    echo "</textarea>";
                    echo "</center>";
                    
                }
                    else if(isset($request->decode) && isset($request->encode)) {
                        echo "<script>alert('ERROR!')</script>";
                    }

    }

}
