<?php

namespace warehouse\Http\Controllers\API;

use warehouse\Http\Controllers\Interfaces\CekBranch;

class SettingBranchController implements CekBranch
{
    private $cabang;

    public function __construct($branch = 8)
    {
      $this->cabang = $branch;
    }

    public function CekBranch()
    {
      return $this->cabang . 'pulsa tersisa.';
    }
}
