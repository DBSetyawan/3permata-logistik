<?php

namespace warehouse\Http\Controllers\Jobs;

use Illuminate\Support\Carbon as Carbon;
use warehouse\Models\Item;
use warehouse\Models\Customer;
use warehouse\Models\Vendor_item_transports;
use warehouse\Models\Item_transport as Customer_item_transports;
use warehouse\Models\Vendor;
use warehouse\Models\Category_cost;
use warehouse\Models\Jobs_transaction_detail;
use warehouse\Models\Jobs_cost;
use warehouse\Models\Jobs_status;
use Validator;
use warehouse\Models\Transports_orders_statused;
use warehouse\Models\Job_transports;
use warehouse\User;
use Illuminate\Http\Concerns\InteractsWithInput;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Storage;
use Auth;
use SWAL;
use warehouse\Models\Company_branchs;
use warehouse\Models\Transport_orders;
use Illuminate\Http\Request;
use warehouse\Http\Controllers\Controller;

class JobsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:3PE[OPRASONAL][TC]|3PL[OPRASONAL][TC]|3PL[OPRASONAL][TC][WHS]|administrator']);
    }
   
    public function index()
    {
        //
    }

    public function download_file_shipment_id(Jobs_transaction_detail $jobsdetail, $id, Request $request)
    {
               
      $file = $jobsdetail->findOrFail($id);
            
       if (file_exists($file->file_list_pod)) { 

            return response()->download($file->file_list_pod);

            swal()
            ->toast()
                ->autoclose(23000)
            ->message("Information","Good luck, your file has been uploaded!","success"); 
            
         
        } else { 
            // return exit( 'Requested file does not exist on our server!' ); 
            //   return redirect()->back()->with("errors","File yang diminta tidak tersedia di server kami, Report developer!");
            swal()
            ->toast()
                ->autoclose(23000)
            ->message("Information","Sorry, file doesn't exist on server!","error"); 
            // return false;
            return redirect()->back();
          
          }

    //   dd($file);
    //     die();
    //   $StoragePath = Storage::url($file->file_list_pod);
        // $StoragePath = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($file->file_list_pod);
        // dd($StoragePath);
        // die();
        // $StoragePaths = Storage::url($StoragePath);
      // ob_clean();
      // ob_flush();
    //   return Storage::download($StoragePath);
    //  $paths = Storage::disk('public')->getAdapter()->applyPathPrefix($file->file_list_pod);
    //  return \Response::download($paths); 
    //   Storage::disk('public')->getAdapter()->applyPathPrefix($file->file_list_pod);

        // if (file_exists($donlod)) { 

        //     return \Response::download($donlod); 
         
        // } else { 
        //     // return exit( 'Requested file does not exist on our server!' ); 
        //     //   return redirect()->back()->with("error","File yang diminta tidak tersedia di server kami, Report developer!");
        //     return false;
          
        //   }

    }

    public function update_file_upload_shipment(Jobs_transaction_detail $jobsdetail, Request $request)
    {

        // $this->validate($request, [

        //         'file' => 'required',
        //         'file.*' => 'mimes:doc,pdf,docx,zip'

        // ]);
        //     $validator = Validator::make($request->all(), [
        //     'id_shipment' => 'required',
        //     'file' => 'required|mimes:doc,pdf,docx,vsc,zip,jpeg,png,jpg,gif,svg|max:2048',
        //   ]);

            if ($request->hasFile('file')) {

                $files = $request->file('file')->getClientOriginalName();
                $name = $request->file('file')->move(public_path().'/files/', $files);
                $fileds = $jobsdetail->findOrFail($request->id_shipment);

                $fileds->file_list_pod = $name;
                $fileds->save();
                
            }
            return response()->json(['status_file' => 'File berhasil disimpan']);

        // die();
    
                # code...
        //   if ($request->hasFile('file')) {
        //     foreach ($request->file('file') as $file) {
        //         # code...
        //         // $fileds = $jobsdetail->findOrFail($request->id_shipment);
        //         $files = $file->getClientOriginalName();
        //         $name = $file->move(public_path().'/files/', $files);
        //         // $fileds->file_list_pod= "$name";
        //         // $fileds->save();
        //     }

        //   }
        // }
        // return response()->json(['status_file' => 'File berhasil disimpan']);
        
        // if ($request->hasFile('file')) {
           
            
        // }
    

       
      

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function delay_search_vendor(Job_transports $tc, Jobs_transaction_detail $jtd,Request $request){
        
        $r_vendorid = $request->get('vendor_j');
            $r_cost = $request->get('cost_');

        $vendor_item_transport = Vendor_item_transports::searchvendoritem($r_vendorid, $r_cost)
                                    ->searchcityvendor($r_vendorid, $r_cost)->get();
        // $vendor_item_transport = Vendor_item_transports::with(['city' => function ($q) use ($r_vendorid, $r_cost) {
        //     $q->searchvendoritem($r_vendorid, $r_cost);
        // }])->get();

        // dd($vendor_item_transport);
        
        $id = Job_transports::select('id')->max('id');
        $YM = Carbon::Now()->format('my');
        $latest_idx_jbs = Job_transports::latest()->first();
        $prefix = Company_branchs::branchjobs(Auth::User()->company_branch_id)->first();
        $jobs = $id+1;
        $jincrement_idx = $jobs;
        if ($id==null) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
        } 
        elseif ($id == 1){
                $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
                $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 1 && $id < 9 ){
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 9){
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 10) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 10 && $id < 99) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 99) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 100) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 100 && $id < 999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id === 999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id === 1000) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 1000 && $id < 9999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 9999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 6-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 10000) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 6-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 6-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }

        $alert_items = Item::where('flag',0)->get();

        $testdata = $jtd->all();
        $data = array();
        foreach ($testdata as $key => $value) {
            # code...
            $data[] = $value->id;
        }

        $fetch_data_all_transaction_detail = Transport_orders::WhereNotin('order_id', $data)
        ->where('by_users', Auth::User()->name)->get();
    
    
        // dd($fetch_data_all_transaction_detail);
        $system_alert_item_vendor = Vendor_item_transports::with('vendors',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $alert_customers = Customer::with('cstatusid')->where('flag',0)->get();
        $Joblistview = Job_transports::with('transport_orders')->get();

        $data_item_alert_sys_allows0 = Customer_item_transports::with('customers',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $prefix = Company_branchs::branchjobs(Auth::User()->company_branch_id)->first();

        return view('admin.jobs.jobs_transport',[
            'menu'=>'Details Shipment Job List',
            'alert_items'=> $alert_items,
            'prefix' => $prefix,
            'fetch_data_jtd' => $fetch_data_all_transaction_detail,
            'system_alert_item_vendor' => $system_alert_item_vendor,
            'system_alert_item_customer' => $data_item_alert_sys_allows0,
            'alert_customers'=> $alert_customers])->with(compact('Joblistview','jobs_order_idx',
            'jobs_idx_latest','latest_idx_jbs','vendor_item_transport')
        );

    }

    /**backup function load vendor */
    // public function vendor_jobs_loader(Vendor $vdnr, Request $request)
    // {
    //     $data = $vdnr->all();
    //     foreach ($data as $query) {
    //        $results[] = ['value' => $query];
    //      }
    //     return response()->json($data);
    
    // }

    public function vendor_jobs_loader(Vendor_item_transports $vdnr, Request $request)
    {
        $data = $vdnr->with('vendors')->get();
        foreach ($data as $query) {
            $results[] = ['value' => $query];
        }
        return response()->json($data);
    }

    /** back up function transporter load if null order id */
    // public function transport_loader(Jobs_transaction_detail $jtd, Transport_orders $tc, Request $request)
    // {
    //     $testdata = $jtd->all();
    //     $data = array();
    //     foreach ($testdata as $key => $value) {
    //         # code...
    //         $data[] = $value->shipment_id;
    //     }

    //     $data = $tc->WhereNotin('order_id',$data)->Where('company_branch_id', Auth::User()->company_branch_id)->get();
    //     foreach ($data as $query) {
    //        $results[] = ['value' => $query];
    //      }
    //     return response()->json($data);
    
    // }

    public function transport_loader(Jobs_transaction_detail $jtd, Transport_orders $tc, Request $request)
    {
        $testdata = $jtd->all();
        $data = array();
        foreach ($testdata as $key => $value) {
            # code...
            $data[] = $value->shipment_id;
        }

        $data = $tc->Where('company_branch_id', Auth::User()->company_branch_id)->get();
        foreach ($data as $query) {
           $results[] = ['value' => $query];
         }
        return response()->json($data);
    
    }

    public function loads_categorys_cost(Request $request)
    {

        $data = Category_cost::whereIn('id', [6])->get();
        foreach ($data as $query) {
           $results[] = ['value' => $query];
        }
        
        return response()->json($data);
    
    }

    public function loads_categorys_cost_of_cost(Request $request)
    {

        $data = Category_cost::whereNotIn('id', [6])->get();
        foreach ($data as $query) {
           $results[] = ['value' => $query];
        }
        
        return response()->json($data);
    
    }

    public function loads_categorys_cost_with_fk(Request $request, $id)
    {

        $cari = $request->load;
        $data = Category_cost::where('id', 'LIKE', "%$cari%")->get();
  
        foreach ($data as $query) {
            $results[] = ['value' => $query ];
          }
        return response()->json($data);
    
    }

    public function load_uuid_job_shipment(Transport_orders $transports, $id)
    {

    $data = $transports->with('customers','cek_status_transaction')->where('id',$id)->Where('company_branch_id', Auth::User()->company_branch_id)->get();

        foreach ($data as $query) {
          $results[] = ['value' => $query ];
        }
      return response()->json($data);
      
    }

    public function loaded_vendor_item_transport_idx(Vendor_item_transports $vendor_item_transports, $id)
    {
      $data = $vendor_item_transports->FindidVendors($id)->get();
      foreach ($data as $query) {
          $results[] = ['value' => $query ];
        }
      return response()->json($data);
    }

    public function loaded_vendor_item_transportx(Vendor_item_transports $vendor_item_transports, $id)
    {
      $data = $vendor_item_transports->FindidVendorsidx($id)->get();
      foreach ($data as $query) {
          $results[] = ['value' => $query ];
        }
      return response()->json($data);
    }
    
    /** backup function if selected shipment if exists on jobs transport details */
    // public function load_uuid_job_shipment_selected(Jobs_transaction_detail $jtd,Transport_orders $transports, $id)
    // {   
    //     $idToArray = explode(",",$id);
    //     $tempArray = array();
        
    //     foreach($idToArray as $key=>$value) {
           
    //         $tempArray[$key+1] = $value;

    //     }
        
    //     $idToArray = $tempArray;

    //     $testdata = $jtd->all();
    //     $data = array();
    //     foreach ($testdata as $key => $value) {
    //         # code...
    //         $data[] = $value->shipment_id;
    //     }

    //     // $datacx = array_merge($idToArray, $data);


    //     // $data = $transports->WhereNotin('order_id', $data)->where('by_users', Auth::User()->name)->whereNotin('id', $idToArray)->get();
    //     $data = $transports->WhereNotin('order_id', $data)->where('company_branch_id', Auth::User()->company_branch_id)->whereNotin('order_id', $idToArray)->get();

    //     return response()->json($data);

        
    //     foreach ($data as $query) {

    //        $results[] = ['value' => $query];
           
    //     }
    //         if ($data->isEmpty()) {
    //             # code...
    //             $data = [];
    //         }

    //     return response()->json($data);

    // }

    public function load_uuid_job_shipment_selected(Jobs_transaction_detail $jtd,Transport_orders $transports, $id)
    {   
        $idToArray = explode(",",$id);
        $tempArray = array();
        
        foreach($idToArray as $key=>$value) {
           
            $tempArray[$key+1] = $value;

        }
        
        $idToArray = $tempArray;

        $testdata = $jtd->all();
        $data = array();
        foreach ($testdata as $key => $value) {
            # code...
            $data[] = $value->shipment_id;
        }

        // $datacx = array_merge($idToArray, $data);


        // $data = $transports->WhereNotin('order_id', $data)->where('by_users', Auth::User()->name)->whereNotin('id', $idToArray)->get();
        $data = $transports->where('company_branch_id', Auth::User()->company_branch_id)->whereNotin('order_id', $idToArray)->get();

        return response()->json($data);

        
        foreach ($data as $query) {

           $results[] = ['value' => $query];
           
        }
            if ($data->isEmpty()) {
                # code...
                $data = [];

            }

        return response()->json($data);

    }

    public function load_uuid_job_shipment_try_load(Jobs_transaction_detail $jtd,Transport_orders $transports, $id)
    {   
        $idToArray = explode(",",$id);
        $tempArray = array();
        
        foreach($idToArray as $key=>$value) {
           
            $tempArray[$key+1] = $value;

        }
            
        $idToArray = $tempArray;

        $testdata = $jtd->all();
        $data = array();
        foreach ($testdata as $key => $value) {
            # code...
            $data[] = $value->shipment_id;

        }

        // $datacx = array_merge($idToArray, $data);

        // return response()->json($datacx);
        // $data = $transports->WhereNotin('order_id',$data)->Where('by_users', Auth::User()->name)->get();
        // $data = $transports->where('order_id', $idToArray)->where('company_branch_id', Auth::User()->company_branch_id)->get();
        // $data = $transports->WhereNotin('order_id', $data)->where('company_branch_id', Auth::User()->company_branch_id)->whereIn('order_id', $idToArray)->get();

        // $data = $transports->where('company_branch_id', Auth::User()->company_branch_id)->whereNotin('order_id', $idToArray)->get();
        $data = $transports->where('company_branch_id', Auth::User()->company_branch_id)->whereIn('order_id', $idToArray)->get();
        
        foreach ($data as $query) {

           $results[] = ['value' => $query];
           
        }
            if (count($data) == 0) {
                # code...
                $data = [];
                // $data = $transports->where('company_branch_id', Auth::User()->company_branch_id)->get();

            }

        return response()->json($data);

    }

    public function add_data_job_cost_without_code_job_shipment(Jobs_transaction_detail $jtdl, Jobs_cost $jcost, Job_transports $jt,Request $request){
    
        // in progress deploy ... 
        $id = $jcost->select('id')->max('id');
        $jcost_plus = $id+1;
        $jcost_increment_id = $jcost_plus;
        if ($id==null) {
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        } 
        elseif ($id == 1){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1 && $id < 9 ){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9){
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 10 && $id < 99) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 99) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 100) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 100 && $id < 999) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 1000) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1000 && $id < 9999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9999) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10000) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }

        $datajobcosts[] = [
            'job_cost_id' => $jcost_id.uniqid(),
            'job_id' => $request->job_id,
            'by_users' => Auth::User()->name,
            'cost_id' => $request->cost_id,
            'cost' => $request->cost,
            'vendor_item_id' => $request->vendor_item_id,
            'note' => $request->note,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $jcost->insert($datajobcosts);

        return response()->json(
            [
                'job_id' => $request->job_id,
                'category_id' => $request->cost_id,
                'cost' => $request->cost,
                'vendor_item_id' => $request->vendor_item_id,
                'note' => $request->note,
                'created_at' => Carbon::now()->format('d-m-Y H:i:s'),
                'updated_at' => Carbon::now()->format('d-m-Y H:i:s'),

            ]
        );

    }

    public function updated_jobtdetail_with_jobcosts_without_job_transport(Job_transports $jtransports,Jobs_cost $jcost, Request $request){
    
        // in progress solved ...   
        //'this update data with where job_no on alocated user[branch only]
        $resultarr = $jtransports->findOrFail($request->job_id);
        $resultarr->vendor_id = $request->vendor_item_id;
        $resultarr->driver_name = $request->driver;
        $resultarr->plate_number = $request->plat;
        $resultarr->driver_number = $request->driver_number;
        $resultarr->document_reference = $request->docref;
        $resultarr->by_users = Auth::User()->name;
        $resultarr->user_of_company_branch_id = Auth::User()->company_branch_id;
        $alivedata = $resultarr->save();

        $arrsd = array();
        foreach($request->cost_id as $key=>$value) {
        
            $arrsd[] = $value;

        }

        $arrvn = array();

            foreach($request->cost_id as $keys =>$cost_id) {
                    $datarvitem[] = [
                        'cost_id' => $request->cost_id[$keys],
                        'job_id' => $request->job_id
                    ];
            }

            $resulsts = $jcost->update($datarvitem);

            return response()->json($resulsts);

        return response()->json(
            [
                'job_transport_id' => $request->shipment_id, //is_array
                'vendor_item_id' => $request->vendor_item_id,
                'cost_id' => $request->cost_id,
                'job_id' => $request->job_id
            ]
        );

    }

    public function update_jcosts(Jobs_cost $jcst, $permid, Request $request)
    {
        $jcsots = $jcst->where('job_cost_id',$permid)->first();
        $jcsots->cost_id = $request->category_cost;
        $jcsots->cost = $request->cost;
        $jcsots->note = $request->noted;
        $jcsots->by_users = Auth::User()->name; //progress
        $jcsots->save();

        return back();
    }

    public function show_data_job_costs(Jobs_cost $tjcosts, $cost_id){
        $test = $tjcosts->jobtransports(Auth::User()->name)
        ->categorycost()->
            vendoritemtransports()->
                with('vendor_item_transports.vendors','job_transports.jobtransactiondetil')->where('job_costs.job_cost_id', $cost_id)
            ->first();

        // in progress showing data with job costs with modal [solved]
            $ShowingDataJcost = $tjcosts->with('job_transports.jobtransactiondetil')
            ->where('job_cost_id', $cost_id)->first();

            // dd($ShowingDataJcost);
        return response()->json($ShowingDataJcost);
        
    }

    public function add_shipment_inv(Jobs_transaction_detail $jtdl, Jobs_cost $jcost, Job_transports $jt,Request $request){

        $latest_idx_jbs = $jtdl->latest()->first();

            $get_vals = $request->get('get_now');
            if(!empty($latest_idx_jts)) {
                $test = $jtdl->create([
                    'job_id' => $request->get('job_id'),
                    'shipment_id' => $request->get('reqshipment'),
                    'file_list_pod' => '',
                    'shipping_to' => $get_vals+1,
                    'status_detail_shipment_id' => 1,
                    'created_at' => Carbon::now(),
                ]);

        } 
            else {
                $test = $jtdl->create([
                    'job_id' => $request->get('job_id'),
                    'shipment_id' => $request->get('reqshipment'),
                    'status_detail_shipment_id' => 1,
                    'file_list_pod' => '',
                    'shipping_to' => $get_vals+1,
                    'created_at' => Carbon::now(),
                ]);
        }
        return response()->json(
            [
                'shipment_id' => $request->get('reqshipment'),
                'job_id' => $request->get('job_id'),
                'uuid' => $test->id,
                'shippersort' => $test->shipping_to,
                'pengirimansort' => $test->shipping_to
            ]
        );

        // swal()
        // ->toast()
        //     ->autoclose(23000)
        // ->message("Information has been saved",'You have successfully, make jobs!"'.$jobs_idx_latest.'',"success"); 

    }

    public function move_shipment_stopping_ENV(Jobs_transaction_detail $jtdl, Request $r, $shipment_index){

        // $check_index = $r->get('indexing_stopping');

        // if ($check_index == 1) {
        //     $jtdl->where('id',$shipment_index)->update(['shipping_to' => 1]);

        //     # code...
        // }
        //  if($check_index == 2){
        //     $jtdl->where('id',$shipment_index)->update(['shipping_to' => 2]);

        // # code...
        // }
        //  if($check_index == 3){
        //     $jtdl->where('id',$shipment_index)->update(['shipping_to' => 3]);

        // # code...
        // }

        // // return $replace_acc;
        // // $data_respon_back_end = $jtdl->findOrFail($shipment_index);
        
        // return response()->json(
        //     [
        //         'posisi-id-saat-stop' => $r->get('uid_stop'),
        //         'posisi_index_saat_stop' => $r->get('indexing_stopping')
        //     ]
        // ); 

    }

    public function update_movements(Jobs_transaction_detail $jtdl, $indexing, Request $r){

        $data = $jtdl->findOrFail($indexing);

        return response()->json(
            [
                'response_data' => $data
                // 'id' => $r->get('ijc'),
                // 'index-track' => $r->get('awal')
            ]
        ); 

    }

    public function move_shipment_ENV(Jobs_transaction_detail $jtdl, Request $retrieve, $shipment_index){
        
        // $check_index = $retrieve->get('indexing_starting');

        // if ($check_index == 1) {
        //         $jtdl->where('id',$shipment_index)->update(['shipping_to' => 1]);

        //     # code...
        // }
        //  if($check_index == 2){
        //     $jtdl->where('id',$shipment_index)->update(['shipping_to' => 2]);

        // # code...
        // }
        //  if($check_index == 3){
        //     $jtdl->where('id',$shipment_index)->update(['shipping_to' => 3]);

        // # code...
        // }

        // return response()->json(
        //     [
        //         'posisi_id_saat_start' => $retrieve->get('uuid_shipment_starting'),
        //         'posisi_index_saat_start' => $retrieve->get('indexing_starting')
        //     ]
        // );

        die();
        $id = $retrieve->get('readdirdatashipper');
        // $tempArray = array();
        
        // foreach($id as $key=>$value) {
           
        //     $tempArray[$key+1] = $value;

        // }
        
        // $id = $tempArray;
        $test_move_shipment = $jtdl->whereIn('id',$id)->get();
        $sdasd = count($test_move_shipment);
        // foreach ($test_move_shipment as $key => $ambil_val) {
            # code...
                // $test[] = $test_move_shipment->shipping_to; 
            
        // }
        if($sdasd == 1){
            // $test = "data masih 1";
            $jtdl->where('id',$retrieve->isadoo)->update(['shipping_to' => 1]);

            // $warehouseTolist = $jtdl->whereIn('id',$id)->first();
            // $warehouseTolist->shipping_to = 1;
            // $warehouseTolist->save();
            // $jtdl->where('id', $id)->update(['shipping_to' => 1]);
        } else {

            $test_move_shipment = $jtdl->where('id',$retrieve->isadoo)->first();
        // $warehouseTolistsd = $jt->where('job_no',$id)->update(['status' => $request->update_data_status_job_shipments_jdx]);
            // dd($test_move_shipment);
            // die();
        //      foreach ($test_move_shipment as $key => $ambil_val) {
        //     # code...
        //          $shipper_idx[] = $ambil_val->shipping_to; 
            
        // }
        // dd($test_move_shipment->shipping_to);
        // die();

            if ($test_move_shipment->shipping_to==1) {
                // return "thats_me sort 1";
                // $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 1)->update(['shipping_to' => 1]);
                $test_move_shipment = $jtdl->whereIn('id',$id)->get();
                // dd($test_move_shipment);
                foreach ($test_move_shipment as $key => $ambil_val) {
                    //     # code...
                             $shipper_idx[] = $ambil_val->shipping_to; 
                        
                    }

                // $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 2)->update(['shipping_to' => 1]);
                if($shipper_idx == 1){
                  $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 1)->update(['shipping_to' => 2]);

                }

                if($shipper_idx == 2){
                    $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 2)->update(['shipping_to' => 1]);

                }

                if($shipper_idx == 3){
                    $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 3)->update(['shipping_to' => 2]);

                }
                // $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 2)->update(['shipping_to' => 1]);

                // $warehouseTolist = $jtdl->whereIn('id',$id);
                // $warehouseTolist->shipping_to = 2;
                // $warehouseTolist->save();
                // $jtdl->where('id', $id)->update(['shipping_to' => 2]);
                

            } 

            // if($test_move_shipment->shipping_to==2) {
            //     // return "thats_me sort 2";
            //     $jtdl->where('id',$retrieve->isadoo)->where('shipping_to', 2)->update(['shipping_to' => 1]);


            //     // $warehouseTolist = $jtdl->whereIn('id',$id);
            //     // $warehouseTolist->shipping_to = 1;
            //     // $warehouseTolist->save();
            // } 
            
        }
          //     return response()->json(
    //         [
    //             'response' => $test_move_shipment,
    //             'asdasd' => $sdasd,
    //             'read-id-shipper' => $retrieve->get('readdirdatashipper'),
    //             'id' => $retrieve->isadoo,
    //             'index_shipper' => $shipment_index
    //         ]
    //     );
    }

      


        // die();
    //     return response()->json(
    //         [
    //             'response' => $test_move_shipment,
    //             'asdasd' => $sdasd,
    //             'read-id-shipper' => $retrieve->get('readdirdatashipper'),
    //             'id' => $retrieve->isadoo,
    //             'index_shipper' => $shipment_index
    //         ]
    //     );
    // }

    public function add_job_shipment_inv(Jobs_transaction_detail $jtdl, Jobs_cost $jcost, Job_transports $jt,Request $request){

        $id = Job_transports::select('id')->max('id');
        $YM = Carbon::Now()->format('my');
        $latest_idx_jbs = Job_transports::latest()->first();
        $prefix = Company_branchs::branchjobs(Auth::User()->company_branch_id)->first();
        $jobs = $id+1;
        $jincrement_idx = $jobs;
        if ($id==null) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
        } 
        elseif ($id == 1){
                $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
                $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 1 && $id < 9 ){
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 9){
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 2-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 10) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 10 && $id < 99) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 99) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 3-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 100) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 100 && $id < 999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id === 999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 4-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id === 1000) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id > 1000 && $id < 9999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 9999) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 6-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 5-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }
        elseif ($id == 10000) {
            $jobs_order_idx = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 6-strlen($jincrement_idx))). $jincrement_idx;
            $jobs_idx_latest = (str_repeat("JOB".$prefix->prefix."TR".$YM.'00', 6-strlen($latest_idx_jbs->id))). $latest_idx_jbs->id;
        }

        $ETA_ = $request->get('eta_');
        $ETD_ = $request->get('_etd');

            // $datajt[] = [
            //     'vendor_id' => NULL,
            //     // 'job_no' => $generated,
            //     'job_no' => $jobs_idx_latest,
            //     'origin_id' => $request->get('Reqcity'),
            //     'destination_id' => $request->get('Reqdestination'),
            //     'by_users' => Auth::user()->name,
            //     'user_of_company_branch_id' => Auth::User()->company_branch_id,
            //     'status' => '1',
            //     'estimated_time_of_delivery' => $ETD_,
            //     'estimated_time_of_arrival' => $ETA_,
            //     'driver_name' => NULL,
            //     'plate_number' => NULL,
            //     'driver_number' => NULL,
            //     'document_reference' => NULL,
            //     'created_at' => Carbon::now(),
            // ];

            $test = Job_transports::create([
            'vendor_id' => NULL,
            // 'job_no' => $generated,
            'job_no' => $jobs_idx_latest,
            'origin_id' => $request->get('Reqcity'),
            'destination_id' => $request->get('Reqdestination'),
            'by_users' => Auth::user()->name,
            'user_of_company_branch_id' => Auth::User()->company_branch_id,
            'status' => '1',
            'estimated_time_of_delivery' => $ETD_,
            'estimated_time_of_arrival' => $ETA_,
            'driver_name' => NULL,
            'plate_number' => NULL,
            'driver_number' => NULL,
            'document_reference' => NULL,
            'created_at' => Carbon::now(),
        ]);

        return response()->json(
            [
                'city_id' => $request->get('Reqcity'),
                'job_gen' => $jobs_idx_latest,
                'id' => $test->id,
                'destination_id' => $request->get('Reqdestination'),
            ]
        );

        // swal()
        // ->toast()
        //     ->autoclose(23000)
        // ->message("Information has been saved",'You have successfully, make jobs!"'.$jobs_idx_latest.'',"success"); 

    }

    public function data_job_shipment_and_data_job_cost_of_cost_merge_function(Jobs_transaction_detail $jtdl, Jobs_cost $jcost, Job_transports $jt,Request $request){
        $id = $jcost->select('id')->max('id');
        $jcost_plus = $id+1;
        $jcost_increment_id = $jcost_plus;
        if ($id==null) {
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        } 
        elseif ($id == 1){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1 && $id < 9 ){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9){
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 10 && $id < 99) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 99) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 100) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 100 && $id < 999) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 1000) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1000 && $id < 9999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9999) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10000) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        
        $latest_idx_jts = $jcost->latest()->first();

        if(!empty($latest_idx_jts)) {
            
                $test = $jcost->create([
                    'job_id' => $request->index_job,
                    'by_users' => Auth::User()->name,
                    'job_cost_id' => $jcost_id.uniqid(),
                    'cost_id' =>$request->cosid,
                    'cost' => $request->cost_pricex,
                    'note' => $request->notedx,
                    'created_at' => Carbon::now()
                ]);

        } 
            else {

                $test = $jcost->create([
                    'job_id' => $request->index_job,
                    'by_users' => Auth::User()->name,
                    'job_cost_id' => $jcost_id.uniqid(),
                    'cost_id' =>$request->cosid,
                    'cost' => $request->cost_pricex,
                    'note' => $request->notedx,
                    'created_at' => Carbon::now()
                ]);
        }


        return response()->json(
            [
                'cost_id' => $request->cosid,
                'job_id' => $request->index_job,
                'cost_price' => $request->cost_pricex,
                'noted' => $request->notedx,
                'uuid' => $test->id,
            ]
        );

    }

    //method remove list tab shipment
    public function deleteremovejobshipment(Jobs_transaction_detail $jtdl, $id){

        $dml = $jtdl->findOrFail($id);
        $dml->delete();
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }

    //method remove list tab transport
    public function deleteremovejobtransportcost(Jobs_cost $skdaosd, $id){
        // $skdaosd->where('driver_name',$id)->delete();
        $jobcost= $skdaosd->findOrFail($id);
        $jobcost->delete();
        // $jcost->where('driver_name', $id)->first()->delete();
        return response()->json([
            'success' => 'Record deleted successfully!',
            'data' => $id
        ]);
    }

    //method remove list tab cost
    public function deleteremovecostofcost(Jobs_cost $sdasdasd, $id){

        $jobcost= $sdasdasd->findOrFail($id);
        $jobcost->delete();
        // $jcost->where('driver_name', $id)->first()->delete();
        return response()->json([
            'success' => 'Record deleted successfully!',
            'data' => $id
        ]);
    }

    public function data_job_shipment_and_data_job_cost_merge_function(Jobs_transaction_detail $jtdl, Jobs_cost $jcost, Job_transports $jt,Request $request){

        $id = $jcost->select('id')->max('id');
        $jcost_plus = $id+1;
        $jcost_increment_id = $jcost_plus;
        if ($id==null) {
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        } 
        elseif ($id == 1){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1 && $id < 9 ){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9){
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 10 && $id < 99) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 99) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 100) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 100 && $id < 999) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 1000) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1000 && $id < 9999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9999) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10000) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        
        $latest_idx_jts = $jcost->latest()->first();

        if(!empty($latest_idx_jts)) {
            
            $test = $jcost->create([
                'job_id' => $request->index_job,
                'by_users' => Auth::User()->name,
                'job_cost_id' => $jcost_id.uniqid(),
                'cost_id' =>$request->cosid,
                // 'vendor_item_id' => $sdpapscll[$oo],
                'vendor_item_id' => $request->vendoridx,
                'cost' => $request->cost_pricex,
                'note' => $request->notedx,
                'driver_name' => $request->driver_named,
                'driver_number' => $request->drive_phones,
                'doc_reference' => $request->docref,
                'plat_number' => $request->plat_numbers,
                'created_at' => Carbon::now()
            ]);

    } 
        else {

            $test = $jcost->create([
                'job_id' => $request->index_job,
                'by_users' => Auth::User()->name,
                'job_cost_id' => $jcost_id.uniqid(),
                'cost_id' =>$request->cosid,
                // 'vendor_item_id' => $sdpapscll[$oo],
                'vendor_item_id' => $request->vendoridx,
                'cost' => $request->cost_pricex,
                'note' => $request->notedx,
                'driver_name' => $request->driver_named,
                'driver_number' => $request->drive_phones,
                'doc_reference' => $request->docref,
                'plat_number' => $request->plat_numbers,
                'created_at' => Carbon::now()
            ]);
    }


        return response()->json(
            [
                // 'noted' => $request->get('noteds'), 'cost_price' => $request->get('cost_price'),
                // 'category_cost_id' => $idToArray, 'data_driver_name' => $listTabArray,
                // 'data_vendor' => $vendor_fetch_id, 'data_plat' => $request->get('dataFetchPlat'),
                // 'driver_number' => $request->get('dataFetchdriver 'Number'), 'datacost_id' => $request->get('rdatajc'),
                // 'document_refrence' => $request->get('dataFetchDocReference')
                'cost_id' => $request->cosid,
                'driver_name' => $request->driver_named,
                'job_id' => $request->index_job,
                'vendor_id' => $request->vendoridx,
                'plat_number' => $request->plat_numbers,
                'driver_phone' => $request->drive_phones,
                'document ref#' => $request->docref,
                'cost_price' => $request->cost_pricex,
                'uuid' => $test->id,
                'noted' => $request->notedx,
            ]
        );

    }


    public function data_job_shipment_and_data_job_cost(Jobs_transaction_detail $jtdl, Jobs_cost $jcost, Job_transports $jt,Request $request){
       
        $date_now = Carbon::now()->format('Y-m-d');
        $latest_idx_jts = $jt->latest()->first();
        $latest_idx_jts0 = $jt->latest()->first();
        $arrNotes = $request->get('noteds');
        $array_ship = $request->get('requestData');
        // $Rarrayvitemtc = $request->get('adspppdjk');
        $vendor_id = $request->get('asdkjasdoo');
        $rdatajprice = $request->get('erpjc');
        $generated = $request->get('gen_code');
        $vend_for_jt = $request->get('vendors_');

        //data transporter
        $driver_name = $request->get('driver_name');
        $plat = $request->get('plat_number');
        $driver_phone = $request->get('driver_phone');
        $document_reference = $request->get('document_reference');
        $ETA_ = $request->get('eta_');
        $ETD_ = $request->get('_etd');

        // return response()->json(array($ETA_,$ETD_,$driver_name,$plat,$driver_phone,$document_reference));
        // die();

        $id = $jcost->select('id')->max('id');
        $jcost_plus = $id+1;
        $jcost_increment_id = $jcost_plus;
        if ($id==null) {
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        } 
        elseif ($id == 1){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1 && $id < 9 ){
            $jcost_id = (str_repeat("JCID".'00', 2-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9){
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 10 && $id < 99) {
            $jcost_id = (str_repeat("JCID".'00', 3-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 99) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 100) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 100 && $id < 999) {
            $jcost_id = (str_repeat("JCID".'00', 4-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id === 1000) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id > 1000 && $id < 9999) {
            $jcost_id = (str_repeat("JCID".'00', 5-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 9999) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        elseif ($id == 10000) {
            $jcost_id = (str_repeat("JCID".'00', 6-strlen($jcost_increment_id))). $jcost_increment_id;
        }
        
            $datajt[] = [
                'vendor_id' => $vend_for_jt,
                // 'job_no' => $generated,
                'job_no' => $jcost_id,
                'by_users' => Auth::user()->name,
                'user_of_company_branch_id' => Auth::User()->company_branch_id,
                'status' => '1',
                'estimated_time_of_delivery' => $ETD_,
                'estimated_time_of_arrival' => $ETA_,
                'driver_name' => $driver_name,
                'plate_number' => $plat,
                'driver_number' => $driver_phone,
                'document_reference' => $document_reference,
                'created_at' => Carbon::now(),
            ];

        $jt->insert($datajt);

        $igensv = implode(",",$vendor_id);
        $vendor_fetch_id = explode(",", $igensv);

        // $sdasdlp = implode(",",$Rarrayvitemtc);
        // $sdpapscll = explode(",", $sdasdlp);

        $idToArrayjrp = implode(",",$rdatajprice);
        $dasdasd = explode(",", $idToArrayjrp);
        $asdasdqedf = implode(",", $arrNotes);
        $sdazzx = explode(",", $asdasdqedf);
        $asdkanskjjd = implode(",",$array_ship);
        $dshpmnet = explode(",",$asdkanskjjd);
        $arrshipment = array();
        $arrcvnsadp = array();
        $arrysjci = array();

        
        // foreach($sdpapscll as $key=>$value) {
            
        //     if (trim($value) === '')
		// 	$value = null;
            
        //     $arrcvnsadp[] = $value;

        // }
        
        // $sdpapscll = $arrcvnsadp;

        foreach($dshpmnet as $key=>$value) {
            
            $arrshipment[] = $value;

        }
        
        $dshpmnet = $arrshipment;

         $datajc = implode(",",$request->get('rdatajc'));
         $idToArray = explode(",",$datajc);
         $tempArray = array();
         $tempArray0 = array();
         $array_set_vnd = array();
         $arrnote = array();

        foreach($dasdasd as $key=>$value) {
            
            $tempArray0[] = $value;

        }
        
        $dasdasd = $tempArray0;

        foreach($vendor_fetch_id as $key=>$value) {
           
            if (trim($value) === 'null')
			$value = null;
            
            $array_set_vnd[] = $value;

        }
        
        $vendor_fetch_id = $array_set_vnd;

        // return response()->json($vendor_fetch_id);
        // die();

        foreach($sdazzx as $key=>$value) {
            
            $arrnote[] = $value;

        }
        
        $sdazzx = $arrnote;
         
        foreach($idToArray as $key=>$value) {
            
            $tempArray[] = $value;
 
        }
         
        $idToArray = $tempArray;
 
        $datarvitem = [];

         
        if(!empty($latest_idx_jts)) {
            $idxs = $jt->select('id')->max('id');
            foreach($idToArray as $oo =>$arrvitemidx) {
                $datarvitem[] = [
                    'job_id' => $idxs,
                    'by_users' => Auth::User()->name,
                    'job_cost_id' => $jcost_id.uniqid(),
                    'cost_id' => $arrvitemidx,
                    // 'vendor_item_id' => $sdpapscll[$oo],
                    'vendor_item_id' => $vendor_fetch_id[$oo],
                    'cost' => $dasdasd[$oo],
                    'note' => $sdazzx[$oo],
                    'created_at' => Carbon::now(),
                ];
            }

        } 
            else {

                foreach($idToArray as $oo =>$arrvitemidx) {

                    $datarvitem[] = [
                        'job_id' => 1,
                        'by_users' => Auth::User()->name,
                        'cost_id' => $arrvitemidx,
                        'job_cost_id' => $jcost_id.uniqid(),
                        // 'vendor_item_id' => $sdpapscll[$oo],
                        'vendor_item_id' => $vendor_fetch_id[$oo],
                        'cost' => $dasdasd[$oo],
                        'note' => $sdazzx[$oo],
                        'created_at' => Carbon::now(),
                    ];

                }

        }

    // $jcost->insert($datarvitem);

    if(!empty($latest_idx_jts0)) {
        $idxssd = $jt->select('id')->max('id');
        foreach ($dshpmnet as $key => $valshipmnt) {
            # code...
            $datashipment[] = [
                'job_id' => $idxssd,
                'shipment_id' => $valshipmnt,
                'created_at' => Carbon::now()
            ];
        }
    } 
        else {

            foreach ($dshpmnet as $key => $valshipmnt) {
                # code...
                $datashipment[] = [
                    'job_id' => 1,
                    'shipment_id' => $valshipmnt,
                    'created_at' => Carbon::now()
                ];
            }

    }

    // $jtdl->insert($datashipment);

    swal()
    ->toast()
        ->autoclose(23000)
    ->message("Information has been saved",'You have successfully, make jobs!"'.$generated.'',"success"); 
   
            return response()->json(
                [
                    'requestData' => $arrNotes, 'rdatajc' => $datajc,
                    'gen_code' => $generated, 'asdkjasdoo' => $vendor_id, 'vendors_' => $vend_for_jt,
                    'erpjc' => $rdatajprice, 'noteds' => $sdazzx
                ]
            );

    }

    public function loaded_named_category_name(Category_cost $category, $idx){
        
        $data = $category->findOrFail($idx);
        
        return response()->json($data);

    }

    public function loaded_named_vendor_name(Vendor $vend, $idx){
        
        $data = $vend->findOrFail($idx);
        
        return response()->json($data);

    }

    public function getValueTransportUser(Jobs_transaction_detail $sdt){
        $useridx = Auth::User()->name;
        $dst = $sdt->searchtcusers($useridx)->get();
        foreach ($dst as $query) {

            $results[] = ['value' => $query->shipment_id];
            
         }
        return response()->json($dst);

    }

    public function shipment_jobs_list(Job_transports $td, Jobs_cost $jobcosts)
    {

        $userid = Auth::User()->name;
        $datajcosts = $jobcosts->jobtransports($userid)
            ->categorycost()->
                vendoritemtransports()->
                    with('vendor_item_transports.vendors','job_transports.jobtransactiondetil')->get();

                    $dataretrieve_x = $td->with('job_costs','jobtransactiondetil')
                                            ->where('user_of_company_branch_id', Auth::User()->company_branch_id)->get();
                    // dd($dataretrieve_x);
        $alert_items = Item::where('flag',0)->get();
        $system_alert_item_vendor = Vendor_item_transports::with('vendors',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $alert_customers = Customer::with('cstatusid')->where('flag',0)->get();
        $Joblistview = Job_transports::with('transport_orders')->get();
        $data_item_alert_sys_allows0 = Customer_item_transports::with('customers',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $prefix = Company_branchs::branchjobs(Auth::User()->company_branch_id)->first();

        return view('admin.jobs.job_list_shipment.job_shipment_lister',[
            'menu'=>'Details Shipment Job List',
            'alert_items'=> $alert_items,
            'prefix' => $prefix,
            'system_alert_item_vendor' => $system_alert_item_vendor,
            'system_alert_item_customer' => $data_item_alert_sys_allows0,
            'alert_customers'=> $alert_customers])->with(compact('Joblistview','jobs_order_idx',
            'jobs_idx_latest','dst','dataretrieve_x','latest_idx_jbs','tds','vendor_item_transport','datajcosts')
        );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Job_transports $jobs)
    {
        return "im looking";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category_cost $catcost, Job_transports $td, Jobs_cost $jobcosts, $id){

        $userid = Auth::User()->name;
        $datajcosts = $jobcosts->jobtransports($userid)
            ->categorycost()->
                vendoritemtransports()->
                    with('vendor_item_transports.vendors','job_transports.jobtransactiondetil')
                ->get();
                $dkaoskd = $td->with('job_costs')->where('job_no',$id)->where('by_users',Auth::User()->name)->get();
        $getVendor = Vendor::all();
        $reqresults = $td->with('jobtransactiondetil')->where('user_of_company_branch_id', Auth::User()->company_branch_id)->get();
        // $reqid_jobs = $td->where('by_users', Auth::User()->name)->where('job_no', $id)->first();

        //progress
        $fetch_data_jobs = $td->with('job_costs.cost_category','jobtransactiondetil')
        ->where('user_of_company_branch_id', Auth::User()->company_branch_id)->where('job_no', $id)->get();
        //end progress

        // dd($fetch_data_jobs);
        $reqid_jobs = $td->with('job_costs.cost_category','jobtransactiondetil')
        ->where('user_of_company_branch_id', Auth::User()->company_branch_id)->where('job_no', $id)->first();
        $fetch_all_data_category = $catcost->all();

        $alert_items = Item::where('flag',0)->get();
        $system_alert_item_vendor = Vendor_item_transports::with('vendors',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $alert_customers = Customer::with('cstatusid')->where('flag',0)->get();
        $Joblistview = Job_transports::with('transport_orders')->get();
        $data_item_alert_sys_allows0 = Customer_item_transports::with('customers',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $prefix = Company_branchs::branchjobs(Auth::User()->company_branch_id)->first();
    
        return view('admin.jobs.detail_job_shipments.vdetail_job_shipments',[
            'menu'=>'Details Shipment Job List',
            'alert_items'=> $alert_items,
            'prefix' => $prefix,
            'system_alert_item_vendor' => $system_alert_item_vendor,
            'system_alert_item_customer' => $data_item_alert_sys_allows0,
            'alert_customers'=> $alert_customers])->with(compact('Joblistview','jobs_order_idx',
            'reqresults','reqid_jobs','fetch_all_data_category','getVendor','fetch_data_jobs','dkaoskd',
            'latest_idx_jbs','tds','vendor_item_transport','datajcosts')
        );
        
    }

    public function show_list_shipment(Job_transports $td, $job_no)
    {
        $id_detail = $td->with('job_costs.cost_category','jobtransactiondetil.transport_shipment_status','status_vendor_jobs')
        ->where('user_of_company_branch_id', Auth::User()->company_branch_id)->where('job_no', $job_no)->get();

        return response()->json($id_detail);

    }


    public function history_job_shipment_lister(Jobs_transaction_detail $jtds,Transport_orders $Transports, Category_cost $catcost, Job_transports $td, Jobs_cost $jobcosts){

        $alert_items = Item::where('flag',0)->get();
        $system_alert_item_vendor = Vendor_item_transports::with('vendors',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $alert_customers = Customer::with('cstatusid')->where('flag',0)->get();
        $data_item_alert_sys_allows0 = Customer_item_transports::with('customers',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $prefix = Company_branchs::branchjobs(Auth::User()->company_branch_id)->first();

        $fetch_data = $jtds->all();
        $iamarraythis = array();

        foreach ($fetch_data as $key => $value) {
            # code...
            $iamarraythis[] = $value->shipment_id;

        }

        $fetch_data = $iamarraythis;

        $check_for_not_available_code = $Transports
        ->whereNotIn('order_id', $fetch_data)->where('company_branch_id', Auth::User()->company_branch_id)->get();

        $asdccdf = array();

        foreach ($check_for_not_available_code as $key => $value) {
            # code...
            $asdccdf[] = $value->order_id;

        }

        $check_for_not_available_code = $asdccdf;
        

        $data_transport = $Transports->with('customers','cek_status_transaction')
        ->orWhereIn('order_id', $check_for_not_available_code)
        ->where('company_branch_id', Auth::User()->company_branch_id)->get(); 
        // dd($data_transport);
        $order_ids = $Transports->with('customers','cek_status_transaction')
        ->where('by_users', Auth::User()->name)->get();
    
        return view('admin.jobs.history_job_shipment.job_shipment_history_list',[
            'menu'=>'Histroy Jobs shipment',
            'alert_items'=> $alert_items,
            'prefix' => $prefix,
            'system_alert_item_vendor' => $system_alert_item_vendor,
            'system_alert_item_customer' => $data_item_alert_sys_allows0,
            'alert_customers'=> $alert_customers])->with(compact('Joblistview','jobs_order_idx',
            'reqresults','reqid_jobs','fetch_all_data_category','getVendor','fetch_data_jobs','dkaoskd',
            'latest_idx_jbs','order_ids','data_transport','check_for_not_available_code','tds','vendor_item_transport','datajcosts')
        );
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $results = Jobs_cost::findOrFail($id)->delete();
        // flash('Information has been deleted.')->success();
        // return redirect('/jobs-forelist-vendor');
                   
    }

    public function soft_deleting_cost(Jobs_cost $jcsots, $jobshipment, $id)
    {
        $dmp = $jcsots->findOrFail($id);
        $results = Jobs_cost::findOrFail($id)->delete();
        swal()
        ->toast()
            ->autoclose(9000)
        ->message("Information has been deleted",'You have successfully order!"'.$dmp->job_cost_id.'',"info"); 
        return back();
        if($results==true){
            $dmp = $jcsots->findOrFail($id);
            $jcsots->by_users = Auth::User()->name;
            $jcsots->save();
        } 
            else {
                return abort(500, "Internal server error.");
        }
                   
    }

    public function find_status_jobs_shipment_idx(Request $request,Jobs_status $js,Job_transports $jts, $id)
    {

        $result_def = $jts->with('status_vendor_jobs')->where('job_no',$id)->get();
            // foreach ($result_def as $datax) {
            //     # code...
            //     $queryz[] = ['value' => $datax];
            // }

            foreach ($result_def as $querys) {
                $results = $querys->status_vendor_jobs->name;
            }

            if ($results=="draft") {
                # code...
                $cari = $request->q;
                $data_for_tc = array('process','delivering','delivered');
                // $data_for_tc = array('draft','cancel','proses','pod');
                $data = $js->select('id', 'name')->whereIn('name', $data_for_tc)->where('name', 'LIKE', "%$cari%")->get();
    
                return response()->json($data);
  
            }

            if ($results=="process") {
                # code...
                $cari = $request->q;
                $data_for_tc = array('process','delivering','delivered');
                // $data_for_tc = array('draft','cancel','proses','pod');
                $data = $js->select('id', 'name')->whereIn('name', $data_for_tc)->where('name', 'LIKE', "%$cari%")->get();
    
                return response()->json($data);
  
            }

            // return response()->json($result_def);

                   
    }

    public function update_status_job_shipment(Request $request,Transport_orders $tc, Job_transports $jt, $id){
       
        $warehouseTolistsd = $jt->where('job_no',$id)->update(['status' => $request->update_data_status_job_shipments_jdx]);

        swal()
        ->toast()
            ->autoclose(23000)
        ->message("Information has been updated",'Status has changed!',"info"); 
        return redirect()->back();
        // return response()->json([
        //     'response' => 'success updated data.'
        // ]);
    }

    public function update_status_transport_order_idx(Request $request,Transport_orders $tc, $id){
       
        $warehouseTolistsd = $tc->where('order_id',$id)->update(['status_order_id' => $request->update_data_status_job_shipments_jdx]);

        return redirect()->back();
        // return response()->json([
        //     'response' => 'success updated data.'
        // ]);
    }


    public function find_status_jobs_shipment_with_transport_shipment_id(Request $request,Transports_orders_statused $tos, Transport_orders $t0,Job_transports $jts, $id)
    {

        $result_def = $t0->where('order_id',$id)->get();
            // foreach ($result_def as $datax) {
            //     # code...
            //     $queryz[] = ['value' => $datax];
            // }

        foreach ($result_def as $querys) {
            $results = $querys->cek_status_transaction->status_name;
        }

        if ($results=="draft") {
            # code...
            $cari = $request->q;
            $data_for_tc = array('proses','invoice');
            // $data_for_tc = array('draft','cancel','proses','pod');
            $data = $tos->select('id', 'status_name')->whereIn('status_name', $data_for_tc)->where('status_name', 'LIKE', "%$cari%")->get();

            return response()->json($data);

        }

        if ($results=="proses") {
            # code...
            $cari = $request->q;
            // $data_for_tc = array('proses');
            $data_for_tc = array('proses','invoice');
            $data = $tos->select('id', 'status_name')->whereIn('status_name', $data_for_tc)->where('status_name', 'LIKE', "%$cari%")->get();

            return response()->json($data);

        }

    }

    public function search_id_is_draft_only(Job_transports $jc,Transport_orders $tc, $job_shipment_order){

        $data_job_cost = $jc->with('status_vendor_jobs')
        ->where('user_of_company_branch_id', Auth::User()->company_branch_id)->where('job_no', $job_shipment_order)->first();

        if ($data_job_cost['status_vendor_jobs']['name'] == "process") {
            
            $array_null = [];
            return response()->json($array_null);

        }
        
        if ($data_job_cost['status_vendor_jobs']['name'] == "draft") {

            $array_all_val = $tc->where('company_branch_id',Auth::User()->company_branch_id)->get();
            return response()->json($array_all_val);
        }

    }

    public function add_shipment_id(Job_transports $td,Jobs_transaction_detail $jtd, Request $request){

        //cek status job status for job shipment with order id
        $dataretrieve_x = $td->with('status_vendor_jobs')
        ->where('user_of_company_branch_id', Auth::User()
        ->company_branch_id)->findOrFail($request->job_id);

        if ($dataretrieve_x['status_vendor_jobs']['name'] == "process") {
            # code...
            // return response()->json(['response' => 'ini adalah proses']);
            return back();
        }

        if ($dataretrieve_x['status_vendor_jobs']['name'] == "draft") {
            # code...
            // return response()->json(['response' => 'ini adalah draft']);
            $datashipment[] = [
                'job_id' => $request->job_id,
                'shipment_id' => $request->order_id,
                'status_detail_shipment_id' => 1,
                'file_list_pod' => '',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
    
            $jtd->insert($datashipment);
    
            return response()->json(
                [
                    'job_id' => $request->job_id,
                    'shipment_id' => $request->order_id,
                    'created_at' => Carbon::now()->format('d-m-Y H:i:s'),
                    'updated_at' => Carbon::now()->format('d-m-Y H:i:s'),
    
                ]
            );

        }

        if ($dataretrieve_x['status_vendor_jobs']['name'] == "delivering") {
            # code...
            // return response()->json(['response' => 'ini adalah delivering']);
            return back();
        }

        if ($dataretrieve_x['status_vendor_jobs']['name'] == "delivered") {
            # code...
            // return response()->json(['response' => 'ini adalah delivered']);
            return back();

        }
  
    }
    
}
