<?php

namespace warehouse\Http\Controllers;

use Auth;
use warehouse\User;
use GuzzleHttp\Client;
use warehouse\Models\Item;
use Illuminate\Http\Request;
use warehouse\Models\Customer;
use GuzzleHttp\Promise\Promise;
use warehouse\Models\Companies;
use warehouse\Models\Company_branchs;
use warehouse\Models\Transport_orders;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Database\Eloquent\Builder;
use warehouse\Models\Vendor_item_transports;
use Illuminate\Foundation\Auth\RedirectsUsers;
use warehouse\Models\APIintractive as dbcheck;
use warehouse\Http\Controllers\TestGenerator\CallSignature;
use warehouse\Http\Controllers\warehouse\WarehouseController;
use warehouse\Http\Controllers\Services\AccuratecloudInterface;
use warehouse\Models\Item_transport as Customer_item_transports;
use warehouse\Http\Controllers\Services\Apiopentransactioninterface;

class ManagementController extends Controller
{

    use RedirectsUsers, CallSignature;

    protected $perusahaan;
    protected $warehouse;
    protected $jagal;
    private $Apiopentransaction;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AccuratecloudInterface $clouds, Apiopentransactioninterface $APInterface, Companies $perusahaantbl, WarehouseController $whs, Request $requested)
    {
        $this->perusahaan = $perusahaantbl;
        $this->warehouse = $whs;
        $this->jagal = $requested;
        $this->Apiopentransaction = $APInterface;
        $this->openModulesAccurateCloud = $clouds;
        $this->middleware(['auth','CekOpenedTransaction','verified']);
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cek_company_by_owner = $this->perusahaan->where('owner_id', Auth::User()->id )->get();

        if ($cek_company_by_owner->isEmpty()) {
            # code...
            $cek_super_user_by_owner = 'undefined';
        } else {
            $cek_super_user_by_owner = 'available';

        }

        $prefix = Company_branchs::branchname(Auth::User()->company_branch_id)->first();
        $system_alert_item_vendor = Vendor_item_transports::with('vendors',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $system_alert_item_customers = Customer_item_transports::with('customers',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $alert_items = Item::where('flag',0)->get();
        $alert_customers = Customer::with('cstatusid')->where('flag',0)->get();

        $fetch_izzy = dbcheck::where('check_is','=','api_izzy')->get();

        foreach ($fetch_izzy as $value) {
            # code...
            $fetchArrays[] = $value->check_is;
        } 

        if(isset($fetchArrays) != null){
            $operations_api_izzy_is_true_v1 = dbcheck::where('check_is','=','api_izzy')->get();

            foreach ($operations_api_izzy_is_true_v1 as $operationz) {
                # code...
                $fetchArray1 = $operationz->operation;
            } 

            $operations_api_izzy_is_true_v2 = dbcheck::where('check_is','=','api_accurate')->get();
            
            foreach ($operations_api_izzy_is_true_v2 as $operations) {
                # code...
                $fetchArray2 = $operations->operation;
            } 

        } 

        $results = null;
        $Authorized = Auth::User()->roles;
        foreach ($Authorized as $key => $checkaccess) {
            # code...
            $results = $checkaccess->name;
        }

        if($results == null){
            return view('admin.index',[
                'menu' => 'Admin Dashboard',
                'system_alert_item_vendor' => $system_alert_item_vendor,
                'system_alert_item_customer' => $system_alert_item_customers,
                'alert_items' => $alert_items,
                'api_v1' => $fetchArray1,
                'api_v2' => $fetchArray2,
                'apis' => $results,
                'alert_customers' => $alert_customers,
                'prefix' => $prefix,
                'cek_super_user_by_owner' => $cek_super_user_by_owner
            ]);
        } else {
            // $getCloudAccurate = $this->openModulesAccurateCloud
            // ->FuncModuleReceivedWebhook();

            // return response()->json($AccurateCloud);
            // Sentry\captureMessage('ada sesuatu yang sala');
              return view('admin.index',[
                'menu' => 'Admin Dashboard',
                'system_alert_item_vendor' => $system_alert_item_vendor,
                'system_alert_item_customer' => $system_alert_item_customers,
                'alert_items' => $alert_items,
                'api_v1' => $fetchArray1,
                'username' => Auth::User()->email,
                'name' => Auth::User()->name,
                // 'webhook' => $getCloudAccurate,
                // 'api_v2' => $fetchArray2,
                'apis' => $results,
                'alert_customers' => $alert_customers,
                'prefix' => $prefix,
                'cek_super_user_by_owner' => $cek_super_user_by_owner
            ]);
        }

    }

    public function promises(){

        // $client = new \GuzzleHttp\Client();
 
        // $promise1 = $client->getAsync('http://loripsum.net/apis')->then(
        //     function ($response) {
        //         return $response->getBody();
        //     }, function ($exception) {
        //         return $exception->getMessage();
        //     }
        // );
         
        // $promise2 = $client->getAsync('http://loripsum.net/apis')->then(
        //     function ($response) {
        //         return $response->getBody();
        //     }, function ($exception) {
        //         return $exception->getMessage();
        //     }
        // );
         
        // $response1 = $promise1->wait();
        // $response2 = $promise2->wait();
         
        // echo $response1;
        // echo $response2;
        // $promise = new Promise(
        //     function () use (&$promise) {
        //         $promise->resolve('waiteds');
        //     },
        //     function () {
        //         // do something that will cancel the promise computation (e.g., close
        //         // a socket, cancel a database query, etc...)
        //     }
        // );
        // return response()->json($promise->wait());
        // assert('waited' === $promise->wait());
    }

    public function findbranchWithRoleBranchId($id)
    {
        $companysbranch = Company_branchs::with('company')->where(function (Builder $query) use($id) {
            return $query->where('id', $id);
        })->get();

        return response()->json($companysbranch);
        // return $id;
    }

    public function OpenTransaction($branch_id)
    {
        $openedIdUser = isset(Auth::User()->id) ? Auth::User()->id : $this->redirectPath();

        // cek jika session/cookie telah bersih/kosong pada posisi user masih membuka transaksi
        // user akan  dikembalikan ke login
        if($openedIdUser == "/dashboard")
            return $this->redirectPath();
        else
        $cek_company_by_owner = $this->perusahaan->where('owner_id', $openedIdUser)->get();

        if ($cek_company_by_owner->isEmpty()) {
            # code...
            $cek_super_user_by_owner = 'undefined';
        } else {
            $cek_super_user_by_owner = 'available';

        }

        $prefix = Company_branchs::branchname($branch_id)->first();
        $system_alert_item_vendor = Vendor_item_transports::with('vendors',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $system_alert_item_customers = Customer_item_transports::with('customers',
        'city_show_it_origin','city_show_it_destination')->where('flag', 0)->get();
        $alert_items = Item::where('flag',0)->get();
        $alert_customers = Customer::with('cstatusid')->where('flag',0)->get();

        $fetch_izzy = dbcheck::where('check_is','=','api_izzy')->get();

        foreach ($fetch_izzy as $value) {
            # code...
            $fetchArrays[] = $value->check_is;
        } 

        if(isset($fetchArrays) != null){
            $operations_api_izzy_is_true_v1 = dbcheck::where('check_is','=','api_izzy')->get();

            foreach ($operations_api_izzy_is_true_v1 as $operationz) {
                # code...
                $fetchArray1 = $operationz->operation;
            } 

            $operations_api_izzy_is_true_v2 = dbcheck::where('check_is','=','api_accurate')->get();
            
            foreach ($operations_api_izzy_is_true_v2 as $operations) {
                # code...
                $fetchArray2 = $operations->operation;
            } 

        } 

        // $request_branchs = $this->Apiopentransaction->getBranchIdWithdynamicChoosenBrach($branch_id);

        $results = null;
        $Authorized = Auth::User()->roles;

        foreach ($Authorized as $checkaccess) {
            # code...
            $results = $checkaccess->name;
        }
        if($results == null){
            // $getCloudAccurate = $this->openModulesAccurateCloud
            // ->FuncModuleReceivedWebhook();
            return view('admin.index',[
                'menu' => 'Admin Dashboard',
                'system_alert_item_vendor' => $system_alert_item_vendor,    
                'system_alert_item_customer' => $system_alert_item_customers,
                'alert_items' => $alert_items,
                'some' => 0,
                'username' => Auth::User()->email,
                'name' => Auth::User()->name,
                // 'webhook' => $getCloudAccurate,
                'api_v1' => $fetchArray1,
                'api_v2' => $fetchArray2,
                'choosen_user_with_branch' => $branch_id,
                'alert_customers' => $alert_customers,
                'prefix' => $prefix,
                'cek_super_user_by_owner' => $cek_super_user_by_owner
            ]);
        } 
            else {
                // $getCloudAccurate = $this->openModulesAccurateCloud
                // ->FuncModuleReceivedWebhook();
                $i = 0;

                foreach (Transport_orders::cursor() as $flight) {
                    // foreach (User::query()->get() as $flight) {
                    //
                    // return $flight;
                    $i++;

                }

                $data = "<h1>Iterated through ".$i." are using ".(round(memory_get_peak_usage() / 1024 / 1024))." MB of memory</h1>";
                return view('admin.index',[
                    'menu' => 'Admin Dashboard',
                    'system_alert_item_vendor' => $system_alert_item_vendor,    
                    'system_alert_item_customer' => $system_alert_item_customers,
                    'alert_items' => $alert_items,
                    'username' => Auth::User()->email,
                    'name' => Auth::User()->name,
                    'api_v1' => $fetchArray1,
                    'api_v2' => $fetchArray2,
                    'apis' => $results,
                    'd' => $data,
                    'choosen_user_with_branch' => $branch_id,
                    'alert_customers' => $alert_customers,
                    'prefix' => $prefix,
                    'cek_super_user_by_owner' => $cek_super_user_by_owner
                ]);

        }

    }

    public function storeValue()
    {   
        $this->jagal->session()->put('branch_id', $this->jagal->responseJsonData);
    }


    public static function callbackme(){
        $fetch_izzy = dbcheck::where('check_is','=','api_izzy')->get();

        foreach ($fetch_izzy as $value) {
            # code...
            $fetchArrays[] = $value->check_is;
        } 

        if(isset($fetchArrays) != null){
            $operations_api_izzy_is_true_v1 = dbcheck::where('check_is','=','api_izzy')->get();

            foreach ($operations_api_izzy_is_true_v1 as $operationz) {
                # code...
                $fetchArray1 = $operationz->operation;
            } 

            $operations_api_izzy_is_true_v2 = dbcheck::where('check_is','=','api_accurate')->get();
            
            foreach ($operations_api_izzy_is_true_v2 as $operations) {
                # code...
                $fetchArray2 = $operations->operation;
            } 

        } 

        return response()->json([
            'api_v1' => $fetchArray1,
            'api_v2' => $fetchArray2
        ]);

    }

    public function spa()
    {
        return view('spa.landing');
    }

}
