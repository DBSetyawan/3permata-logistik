<?php

namespace warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Order_transport_history extends Model
{
    protected $table = "order_transport_histories";
    protected $fillable = ['id','order_id','datetime','job_no','user_id','status','created_at','updated_at'];

    public function user_order_transport_history()
    {
        return $this->belongsTo('warehouse\User','user_id');
    }

    public function statusHistoryName()
    {
        return $this->belongsTo('warehouse\Models\Transports_orders_statused','status');
    }

}
