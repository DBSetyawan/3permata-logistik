<?php

namespace warehouse\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Webhook extends Model
{
    /**
     *
     * @param $message
     *
     * @return mixed
     */
    public function postMessage( $message ) {

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->post( $url,
            ['body' => $message]
        );

        $data = json_decode( $response->getBody()->getContents() );

        return $data;
    }

}
