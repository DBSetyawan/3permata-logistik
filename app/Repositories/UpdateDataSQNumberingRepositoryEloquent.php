<?php

namespace warehouse\Repositories;

use warehouse\Repositories\AccurateCloudRepos;
use warehouse\Http\Controllers\TestGenerator\CallSignature;
use warehouse\Http\Controllers\Services\AccurateCloudmodules;

class AccurateCloudRepositoryEloquent extends AccurateCloudmodules implements AccurateCloudRepos
{   
    use CallSignature;
    
    protected $moduleAccurateCloud;
    
    /**
    * @method form params
    * https://laravel.com/api/5.7/Illuminate/Http/JsonResponse.html
    */
    public function AccurateCloudpatternDBLIST()
    {

        return response()->json(
            ['data' =>$this->FuncOpenmoduleAccurateCloudDblist(
                            $this->_TS()
                        )
                    // ->original['d'][0]['trialEnd']
                    ->original['d'][0]
                    // ->original['d'][0]
            ]
        );

    }

    /**
     * search primary id SQ number
      * @method GET 
    */
    public function getSQnumber()
    {
        $id = $this->FuncOpenmoduleAccurateCloudListSalesQoutation('SQIT1-SH1912200034');
        return $this->FuncOpenmoduleAccurateCloudDetailSalesQoutation($id, $this->_TS());
        // return $id;
    }

    /**
     * update primary id SQ number
      * @method POST 
    */
    public function updateSQnumber()
    {
        
    }

    /**
     * search primary id SO number
      * @method GET 
    */
    public function getSOnumber()
    {
        
    }
    
    /**
     * update primary id SO number
      * @method POST 
    */
    public function updateSOnumber()
    {
        
    }

    /**
     * update primary id DO number
      * @method POST 
    */
    public function getDOnumber()
    {
        
    }

    /**
     * update primary id DO number
      * @method POST 
    */
    public function updateDOnumber()
    {
        
    }

    /**
     * @callable times 
      * @method POST 
      * https://laravel.com/api/5.7/Illuminate/Http/JsonResponse.html
    */
    protected function _TS()
    {
        return $this->GeneratorFetchJustTime()->getOriginalContent()['_ts'];
    }
    
}
