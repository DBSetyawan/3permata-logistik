@inject('Contracts','warehouse\Http\Controllers\Services\Apiopentransactioninterface')
@php
    $roles_branch = isset($choosen_user_with_branch) ? $choosen_user_with_branch : null;
    $rebranch = $Contracts->getBranchIdWithdynamicChoosenBrach($roles_branch);
@endphp
@extends('admin.layouts.master', array('some'=>$roles_branch))
@section('title','Dashboard')
@section('head')
<link rel="shortcut icon" href="{{ asset('img/logo.ico') }}" />
<link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/bootstrap/css/bootstrap-fileupload.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
{{-- <link href="{{ asset('css/empty-val/emptys.css') }}" rel="stylesheet" /> --}}
<link href="{{ asset('css/hunterPopup.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/like-yt.css') }}" />
<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/gritter/css/jquery.gritter.css') }}" />
<link href="{{ asset('css/style-default.css') }}" rel="stylesheet" id="style_color" />
<link href="{{ asset('assets/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/select2.4.0.3/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/uniform/css/uniform.default.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/sweet-alert2/sweet-alert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/allowed-all-css/all-alias-css.css') }}">
<link href="{{ asset('assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css" media="screen"/>
{{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
<style>
.empty-state {
    width: 96%;
    position: relative;
    -webkit-border-radius: 4px;
    border-radius: 4px;
    border: 2px dashed #eee;
    text-align: center;
    padding: 10px 20px;
    margin: 10px 0
}
</style>
@notifyCss
@endsection
@section('brand')
<a class="brand" href="{{url('home_admin')}}">
    <img src="../img/logo.png" alt="Metro Lab" />
</a>
@endsection
@section('breadcrumb')
 <li>
    <a href="/dashboard"><i class="icon-home"></i></a>
    <span class="divider">/</span>
</li>
<li>Branch, scope place :
    <strong class="cabang">{{ isset($rebranch->branch) ? $rebranch->branch : "Not found branch, (404)" }}</strong>
    <span class="divider">/</span>
</li> 
<li>Privacy, access point :
        @foreach (Auth::User()->roles as $role_users)
            <a href="#"> [ <strong class="roles">{{$role_users->name}}</strong> ]</a>
        @endforeach
        <span class="divider">/</span>
    </li> 
<li class="active">
{{ __('Login as:') }} <strong>{{Auth::user()->name}}</strong>
</li>
@endsection

@section('content')
{{-- {{ isset($choosen_user_with_branch) ? $choosen_user_with_branch : null; }} --}}
<div id="main-content">
    <div id="progress" class="waiting">
        <dt></dt>
        <dd></dd>
    </div>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
       <!-- BEGIN PAGE HEADER-->
       <div class="row-fluid">
          <div class="span12">
              <!-- BEGIN THEME CUSTOMIZER-->
              {{--  <div id="theme-change" class="hidden-phone">
                  <i class="icon-cogs"></i>
                   <span class="settings">
                       <span class="text">Theme Color:</span>
                       <span class="colors">
                           <span class="color-default" data-style="default"></span>
                           <span class="color-green" data-style="green"></span>
                           <span class="color-gray" data-style="gray"></span>
                           <span class="color-purple" data-style="purple"></span>
                           <span class="color-red" data-style="red"></span>
                       </span>
                   </span>
              </div>  --}}
              <!-- END THEME CUSTOMIZER-->
             <!-- BEGIN PAGE TITLE & BREADCRUMB-->
              <h3 class="page-title">
                   @yield('title')
              </h3>
              <ul class="breadcrumb">
                  @yield('breadcrumb')
              </ul>
              <div class="empty-state">
                This application is connected to each other as a transportation information management and is significantly integrated
              </div>
              <pre id="json"></pre>
              {{-- @if ($roles_branch == null)
             
                <div>Grafing </div>
                  
              @else 
              <div class="span5">
                    <canvas id="myChart" width="400" height="400"></canvas>
                  </div>
              @endif --}}
              <!-- END PAGE TITLE & BREADCRUMB-->
              {{-- {{ $d }} --}}

          </div>
       </div>
       @inject('branchs', 'warehouse\Models\company_branchs')
       @php $cabang = $branchs->whereIn('company_id',[session()->get('company_id')])->first(); $rollback = $cabang['branch']; @endphp
       @include('flash::message')
       @if (\Session::has('success'))
       <div id="success" class="alert alert-success fade in">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <h4 class="alert-heading">#Branch is rollback : {{ $rollback }} </h4><br />
            <p>{{ \Session::get('success') }}</p>
        </div>
       @endif
       @if (\Session::has('error'))
        <div id="error" class="alert alert-block alert-error fade in">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <h4 class="alert-heading">#Code branch (403) Access denied</h4><br />
            <p>{{ \Session::get('error') }}</p>
        </div>
        @endif
    </div>
    <!-- END PAGE CONTAINER-->
 </div>
@endsection

@section('javascript')
{{-- <script src="{{ mix('js/app.js') }}"></script> --}}
{{-- <script src="https://unpkg.com/vue@2.6.10/dist/vue.js"></script> --}}
{{-- <script src="{{ asset('js/src-vue/layers-runner.js') }}"></script>
<script src=" {{ asset('js/sweet-alerts/sweet-alerts.min.js')}}"></script> --}}
@include('sweetalert::view')
@include('cookieConsent::index')
@include('notify::messages')
@notifyJs
    <script src="{{ asset('js/jquery-popup.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
   <script src="{{ asset('js/jquery-1.8.3.min.js') }}"></script>
   <script src="{{ asset('js/jquery.nicescroll.js') }}" type="text/javascript"></script>
   <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
   <script src="{{ asset('js/jquery.blockui.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
   <script src="{{ asset('js/jquery.sparkline.js') }}" type="text/javascript"></script>
   <script type="text/javascript" src="{{ asset('assets/data-tables/jquery.dataTables.js') }}"></script>
   <script type="text/javascript" src="{{ asset('assets/data-tables/DT_bootstrap.js') }}"></script>
   <script src="{{ asset('assets/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
   <script src="{{ asset('js/common-scripts.js') }}"></script>
   {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script> --}}
   <script src="{{ asset('js/warehouse_t_list.js') }}"></script>
   {{-- https://www.chartjs.org/ https://www.chartjs.org/samples/latest/ --}}
   {{-- <script src="https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js"></script> --}}
   {{-- <script src="http://127.0.0.1:4200/socket.io/socket.io.js"></script> --}}
   <script language="javascript" type="text/javascript">
   var code = {
        "s": "Accurate Cloud",
        "d": [
            {
                "trialEnd": "16/01/2020"
            }
        ]
   }
    document.getElementById("json").innerHTML = JSON.stringify(code, undefined, 2);

    // ScrollReveal({ reset: true });
    // ScrollReveal().reveal(target, options);
//     var ctx = document.getElementById('myChart').getContext('2d');
// var myChart = new Chart(ctx, {
//     type: 'bar',
//     data: {
//         labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
//         datasets: [{
//             label: '# of Votes',
//             data: [12, 19, 3, 5, 2, 3],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 'rgba(153, 102, 255, 1)',
//                 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         scales: {
//             yAxes: [{
//                 ticks: {
//                     beginAtZero: true
//                 }
//             }]
//         }
//     }
// });
   </script>
@endsection 