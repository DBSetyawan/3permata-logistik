<!DOCTYPE html>
<html>
    {{-- https://tailwindcss.com/docs/top-right-bottom-left https://tailwindcss.com/components --}}
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{csrf_token()}}">
<title>[3PS][BUG] Alert Applications </title>
<link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
    <body>
        <div id="app">
        </div>
              <div class="relative h-20 p-4">
                <div class="xl:bottom-0 xl:inset-x-0">
                    <div style="cursor: pointer" class="p-2 bg-indigo-700 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex" id="alert" role="alert">
                        <span class="flex rounded-full bg-red-500 uppercase px-2 py-1 text-xs font-bold mr-3">Error</span>
                        <span class="font-semibold mr-2 text-left flex-auto">The application detected a system error, contact the developer</span>
                        <svg class="fill-current opacity-75 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg>
                      </div>
                </div>
                <br/>
                <div class="container px-10">
                    <a  href="{{ route('dashboard') }}" class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">
                        Back to home
                      </a>
                    <!-- ... -->
                  </div>
                  
              </div>
              @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
              <script src="{{ mix('js/app.js') }}"></script>
            <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js" crossorigin></script>
            <script>
                 $(function(){
                    $('#alert').click(function (e) {
                        e.preventDefault();
                        Raven.showReportDialog({
                            eventId: '{{ Sentry::getLastEventID() }}',
                            dsn: 'https://64f25e71c2d348f1bf4ac164c25a721e@sentry.io/1354748',
                            user: {
                                'name': 'Daniel Budi Setyawan',
                                'email': 'artexs@programmer.net',
                            }
                        });
                    });
                });
            </script>
        @endif
    </body>
    </html>
 </body>