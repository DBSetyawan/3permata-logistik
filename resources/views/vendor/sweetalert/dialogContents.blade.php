<div class="js-cookie-consent cookie-consent">

    <div class="js-cookie-consent cookie-consent">

        <div class="row d-flex align-items-center text-center">
    
            <div class="col-9 text-left">
                <span class="cookie-consent__message">
                    {!! trans('cookieConsent::texts.message') !!}
                </span>
            </div>
    
            <div class="col-3 text-center">
                <button class="btn btn-light btn-sm js-cookie-consent-agree cookie-consent__agree">
                    {{ trans('cookieConsent::texts.agree') }}
                </button>
                </div>
    
        </div>
    
    </div>

</div>
