<div class="modal fade" id="add_item" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog"
aria-labelledby="add_item" aria-hidden="true" style="margin:-10px -300px;width:600px;height: 624px;display: none">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel1">Add Item Customers</h3>
</div>
        <div class="modal-body" style="max-height:568px;">
            <form class="form-horizontal" id="form_item_sub_services">
                <br />
                {{-- in progress updated vendor --}}
                <div class="control-group hidden">
                    <label class="control-label" style="text-align: end"></label>
                    <div class="controls">
                        <input class="input-large validate[required]" readonly="enabled" type="hidden" maxlength="30" id="itemcode" name="itemcode" value="{{ $jobs_order_idx }}"/>
                        {{-- <span class="help-inline">Some hint here</span> --}}
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" style="text-align: end">Customer</label>
                    <div class="controls">
                        <input class="input-large validate[required]" readonly="enabled" type="text" style="width:345px" maxlength="30" id="customerx" name="customerx"/>
                        <input class="input-large validate[required]" readonly="enabled" type="hidden" maxlength="30" id="customerx_id" name="customerx_id"/>
                        {{-- <select class="dtcstmers input-large m-wrap validate[required]" style="width:224px" tabindex="1" id="customerx" name="customerx">
                        </select> --}}
                </div>
            </div>
                <div class="control-group">
                    <label class="control-label" style="text-align: end">Sub Service</label>
                    <div class="controls">
                        <select class="dtsubservices input-large m-wrap validate[required]" style="width:350px" tabindex="1" id="sub_service_id" name="sub_service_id">
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="text-align: end">Shipment category</label>
                <div class="controls">
                    <select class="dtshipmentctgry input-large m-wrap validate[required]" style="width:350px" tabindex="1" id="shipmentx" name="shipmentx">
                </select>
            </div>
        </div>
            <div class="control-group">
                <label class="control-label" style="text-align: end">Moda</label>
                <div class="controls">
                    <select class="dtmoda input-large m-wrap validate[required]" style="width:350px" tabindex="1" id="moda_x" name="moda_x">
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" style="text-align: end">Origin</label>
            <div class="controls">
                <select class="citys input-large m-wrap validate[required]" style="width:350px" tabindex="1" id="originx" name="originx">
            </select>
        </div>
        </div>
        <div class="control-group">
        <label class="control-label" style="text-align: end">Destination</label>
        <div class="controls">
                <select class="citys input-large m-wrap validate[required]" style="width:350px" tabindex="1" id="destination_x" name="destination_x">
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" style="text-align: end">Item Description</label>
                <div class="controls">
                    <textarea class="input-large validate[required]" style="width:350px" type="text" maxlength="200" id="itemovdesc" name="itemovdesc"></textarea>
                        {{-- <span class="help-inline">Some hint here</span> --}}
                </div>
            </div>
                <div class="control-group">
                    <label class="control-label" style="text-align: end">Unit</label>
                    <div class="controls">
                    {{-- <input class="input-large validate[required]" type="text" maxlength="30" id="unit" name="unit" /> --}}
                        {{-- <span class="help-inline">Some hint here</span> --}}
                        <select class="input-small m-wrap units" data-trigger="hover" style="width:350px" data-content="WOM" data-original-title="Information" id="unit" name="unit">
                            <option value="Rit">Rit</option>
                            <option value="M³">M³</option>
                            <option value="Kg">Kg</option>
                            <option value="Koli">Koli</option>
                        </select>
                </div>
            </div>
        <div class="control-group">
            <label class="control-label" style="text-align: end">Price</label>
            <div class="controls">
            <input class="input-large validate[required]" type="text" maxlength="30" id="price" style="width:350px" name="price" />
                {{-- <span class="help-inline">Some hint here</span> --}}
        </div>
    </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button id="add_item_customer" type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>