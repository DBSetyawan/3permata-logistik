@extends('admin.layouts.master')
@section('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('img/logo.ico') }}" />
<link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/bootstrap/css/bootstrap-fileupload.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link href="{{ asset('css/style.css') }}" rel="stylesheet" />
<link href="{{ asset('css/nprogress.css') }}" rel="stylesheet" />
<link href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/hunterPopup.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/like-yt.css') }}" />
<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/gritter/css/jquery.gritter.css') }}" />
<link href="{{ asset('css/style-default.css') }}" rel="stylesheet" id="style_color" />
<link href="{{ asset('assets/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/select2.4.0.3/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/uniform/css/uniform.default.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" />
<link href="{{ asset('css/datergpickercstm.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link href="{{ asset('css/awesome/awesome-font.css') }}" rel="stylesheet" />
@notifyCss
<style>
        #loader {
          position: absolute;
          left: 50%;
          top: 50%;
          z-index: 1;
          width: 50px;
          height: 50px;
          margin: -75px 0 0 -75px;
          border: -2px solid #f3f3f3;
          border-radius: 50%;
          border-top: -2px solid #3498db;
          -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        }
        
        .animate-bottom {
          position: relative;
          -webkit-animation-name: animatebottom;
          -webkit-animation-duration: 1s;
          animation-name: animatebottom;
          animation-duration: 1s;
         
        }
        
        @-webkit-keyframes animatebottom {
          from { bottom:-100px; opacity:0 } 
          to { bottom:0px; opacity:1 }
        }
        
        @keyframes animatebottom { 
          from{ bottom:-100px; opacity:0 } 
          to{ bottom:0; opacity:1 }
        }
        #myDiv {
          display: none;
        }
        </style>
@endsection
@section('brand')

<a class="brand" href="/home">
    {{-- <img src="../img/logo.png" alt="Tiga Permata System" /> --}}
</a>
@endsection
@section('breadcrumb')
<li>
    <a href="/home"><i class="icon-home"></i></a>
    <span class="divider">/</span>
</li>
<li>
    <a href="{{ route('transport.static', $some) }}">Transport Order List</a>
    <span class="divider">/</span>
</li>
<li class="active">
    {{ $menu}}
</li>
@endsection

@section('content')

<div id="main-content" style="height: 1335px;">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        {{-- <a href="javascript:;" class="btn btn-success" id="add-sticky">Sticky</a> --}}
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    {{ $menu }}
                </h3>
                <ul class="breadcrumb">
                    @yield('breadcrumb')
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        @include('flash::message')
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        @endif
        @if (\Session::has('error'))
        <div class="alert alert-danger">
          <p>{{ \Session::get('error') }}</p>
        </div>
       @endif
        <div id="progress" class="waiting">
            <dt></dt>
            <dd></dd>
        </div>

{{-- <p class="animated infinite bounce delay-5s">ekodaskdoaskdo</p> --}}
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <div class="widget blue">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> {{ $menu }}</h4>
                        <span class="tools">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
                        </span>
                    </div>
                    <div class="widget-body" id="transport-form">
                        <form id="transport_order" class="form-horizontal">
                            {{-- <form id="transport_order" action="{{ route('transport.stored.static', $some) }}" class="form-horizontal" method="POST"> --}}
                            @csrf
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" class="span12 " />
                            <div id="tabsleft" class="tabbable tabs-left">
                                <ul>
                                    <li><a href="#tabsleft-tab1" data-toggle="tab"><span class="strong">Transport Information</span> <span class="muted">Information Service<div>Service Customer Details</div></span></a></li>
                                    <li><a href="#tabsleft-tab2" data-toggle="tab"><span class="strong">Transport Registration</span> <span class="muted">Transport <div>Transport Order Details</div></span></a></a></li>
                                </ul>
                                <div class="progress progress-info progress-striped">
                                    <div class="bar"></div>
                                </div>
                                <div class="tab-content">
                                    <label class="control-label span12 error"><i class="icon-exclamation-sign popovers alert-danger"></i> Tanda ini harus wajib diisi..</label>
                                    <div class="tab-pane" id="tabsleft-tab1">
                                    <div class="row-fluid">
                                            <div class="span6">
                                                <div class="control-group">
                                                    <label class="control-label">Company Branch</label>
                                                    <div class="controls">
                                                        <label style="color: black;font-family: Fira Code">{{ $request_branchs->branch }}</label>
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                                <br>
                                                            </div>
                                                        </iv>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="control-group" style="position:relative">
                                                        <div class="controls controls-row">
                                                        <select class="customer_names form-control validate[required]" id="customers_name" style="width:318px;" name="customers_name" required>
                                                        </select><span class="add-on">
                                                            <a type="button" data-placement="top" id="modal" data-toggle="modal" data-target="#add_customer" data-whatever="" class="btn btn-success"><i class="icon-plus"></i> Add Customer</a>
                                                            {{-- <a id="new_customer" type="button" class="btn btn-default popovers" data-trigger="hover" data-content="Tambahkan, jika customer tidak ditemukan." data-original-title="Informasi User" onclick="location.href='{{ url('/customer/registration') }}'"><i class="icon-plus"></i> Add New Customer</a> --}}
                                                        </span>
                                                        <label style="position: stic;margin:-20px -180px;">Customer Names</label>
                                                            <label id="customers_errors" class="control-label error">
                                                            <i style="position: absolute;margin:-5px -15px;" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Customer harus wajib diisi.." data-original-title="Informasi User"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label"></label>
                                                <div class="controls controls-row">
                                                    <input type="hidden" class="input-block-level validate[required]" placeholder="Enter Origin PIC PHONE" id="id_project"  name="id_project" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <br />
                                </div>
                                <div class="tab-pane" id="tabsleft-tab2">
                                            <div class="row-fluid">
                                                    <div class="span12">
                                                        <hr>
                                                    </div>
                                                </div>
                                        <div style="text-align:right;"><a type="button" data-placement="top" id="modal_address_book" data-toggle="modal" href="#add" data-target="#add_address_book" data-whatever="" class="btn btn-success"><i class="icon-plus"></i> Address book</a></div>
                                            <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="control-group">
                                                            <label class="control-label" style="font-family: 'Courier', monospace;font-size:16px;font: bold;"><strong>Detail&nbsp;Transport</strong></label>
                                                            <div class="controls">
                                                                <hr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <div class="control-group" style="margin:0px -70px">
                                                        <label class="control-label" style="margin:0px 80px;position:absolute">Saved Origin</label>
                                                        <div class="controls">
                                                            <select data-placeholder="Saved Origin" style="width:335px;position:absolute;margin:0px" class="saved_origin" tabindex="-1" name="saved_origin" id="saved_origin">
                                                            </select>
                                                            <span class="add-on">
                                                                <a type="button" data-placement="top" id="modal_reset" data-toggle="modal_rest" href="#reset" data-target="#" data-whatever="" class="btn btn-danger"><i class="icon-remove"></i></a>
                                                                {{-- <a id="new_customer" type="button" class="btn btn-default popovers" data-trigger="hover" data-content="Tambahkan, jika customer tidak ditemukan." data-original-title="Informasi User" onclick="location.href='{{ url('/customer/registration') }}'"><i class="icon-plus"></i> Add New Customer</a> --}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="span6">
                                                    <div class="control-group" style="margin:0px -40px">
                                                        <label class="control-label" style="margin:0px 50px;position:absolute">Saved Destination</label>
                                                        <div class="controls">
                                                            {{-- <select data-placeholder="Saved Destisnation" style="width:304px" class="saved_destination validate[required]" tabindex="-1" name="saved_destination" id="saved_destination"> --}}
                                                            <select data-placeholder="Saved Destination" style="width:300px;position:absolute;margin:0px" class="saved_destination" tabindex="-1" name="saved_destination" id="saved_destination">
                                                            </select>
                                                            <span class="add-on">
                                                                <a type="button" data-placement="top" id="modal_drest" data-toggle="reset-destination" href="#reset_destination" data-target="#" data-whatever="" class="btn btn-danger"><i class="icon-remove"></i></a>
                                                                {{-- <a id="new_customer" type="button" class="btn btn-default popovers" data-trigger="hover" data-content="Tambahkan, jika customer tidak ditemukan." data-original-title="Informasi User" onclick="location.href='{{ url('/customer/registration') }}'"><i class="icon-plus"></i> Add New Customer</a> --}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            &nbsp;
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <div class="control-group" style="position:relative">
                                                            <label id="origin_error" class="control-label error"><i style="margin:0px 96px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Origin harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                <div class="controls controls-row">
                                                                <input type="text" class="input-block-level" style="width:374px;position:relative;margin:0px -70px" placeholder="Enter Origin" maxlength="40" id="origin"  name="origin" required>
                                                                <label style="position: absolute;margin:-20px -165px;">Origin</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6">
                                                        <div class="control-group" style="position:relative">
                                                           <label id="destination_error" class="control-label error"><i style="margin:10px 127px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Destination harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                <div class="controls controls-row">
                                                                <input type="text" class="input-block-level" style="width:339px;position:relative;margin:0px -40px" placeholder="Enter Destination" maxlength="40" id="destination"  name="destination" required>
                                                                <label style="position: absolute;margin:-21px -165px;">Destination</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label style="position:absolute;margin:8px 14px">Origin City</label>
                                                                <label id="origin_city_error" style="margin:0px;" class="control-label error"><i style="margin:0px 96px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Origin City harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                    <div class="controls" style="position:absolute;margin:0px 110px">
                                                                        <select class="loader_city originloaders" style="width:374px" id="origin_city" name="origin_city" required>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <div class="span6">
                                                        <div class="control-group">
                                                        <label style="position:absolute;margin:5px 14px">Destination City</label>
                                                        <label id="destination_city_error" class="control-label error"><i style="margin:0px 127px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Origin City harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                            <div class="controls" style="position:absolute;margin:0px 140px">  
                                                                <select class="loader_city destinationloaders" style="width:340px" id="destination_city" style="width:304px;" name="destination_city" required>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- &nbsp; --}}
                                                <div class="append_this"></div>
                                                <div class="row-fluid hidden">
                                                    <div class="span6">
                                                            <div class="control-group">
                                                                <label class="control-label" >Id Origin City</label>
                                                                <div class="controls controls-row">
                                                                    <input type="text" class="input-block-level" placeholder="Enter Origin City" id="id_origin_city" name="id_origin_city" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="span6">
                                                        <div class="control-group">
                                                            <label class="control-label" >Id Destination City</label>
                                                            <div class="controls controls-row">
                                                                <input type="text" class="input-block-level" placeholder="Enter Destination City" id="id_destination_city" name="id_destination_city" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="row-fluid">
                                                        <div class="span6">
                                                                <div class="control-group" style="position:relative">
                                                                    <label id="origin_address_error" class="control-label error"><i style="margin:0px 96px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Origin Adrress harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                        <div class="controls controls-row">
                                                                        <input type="text" class="input-block-level" style="width:374px;position:relative;margin:0px -70px"placeholder="Enter Address" id="origin_address"  name="origin_address" required>
                                                                        <label style="position: absolute;margin:-20px -165px;">Address</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <div class="span6">
                                                            <div class="control-group" style="position:relative">
                                                                <label id="destination_address_error" class="control-label error"><i style="margin:0px 127px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Destination Adrress harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                    <div class="controls controls-row">
                                                                    <input type="text" class="input-block-level" style="width:339px;position:relative;margin:0px -40px" placeholder="Enter Address" id="destination_address"  name="destination_address" required>
                                                                <label style="position: absolute;margin:-20px -165px;">Address</label>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <div class="control-group" style="position:relative">
                                                            <label id="pic_phone_origin_errors" class="control-label error"><i style="margin:-20px 96px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Origin Pic Phone harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                <div class="controls controls-row">
                                                                <input type="text" class="input-block-level input-phone-origin" style="width:374px;position:relative;margin:0px -70px" placeholder="Enter Phone" id="pic_phone_origin"  name="pic_phone_origin" required>
                                                                <label style="position: absolute;margin:-20px -165px;">Phone</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6">
                                                        <div class="control-group" style="position:relative">
                                                            <label id="pic_phone_destination_errors" class="control-label error"><i style="margin:0px 127px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Destination PIC PHONE harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                <div class="controls controls-row">
                                                                <input type="text" class="input-block-level input-phone-destination" placeholder="Enter Phone" style="width:339px;position:relative;margin:0px -40px" id="pic_phone_destination"  name="pic_phone_destination" required>
                                                                <label style="position: absolute;margin:-20px -165px;">Phone</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span6">
                                                        <div class="control-group" style="position:relative">
                                                            <label id="origin_pic_name_errors" class="control-label error"><i style="margin:0px 96px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="PIC harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                <div class="controls controls-row">
                                                                <input type="text" class="input-block-level" style="width:374px;position:relative;margin:0px -70px" placeholder="Enter PIC NAME" id="pic_name_origin"  name="pic_name_origin"required>
                                                                <label style="position: absolute;margin:-20px -165px;">PIC NAME</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span6">
                                                        <div class="control-group" style="position:relative">
                                                            <label id="destination_pic_name_errors" class="control-label error"><i style="margin:0px 127px" class="icon-exclamation-sign popovers alert-danger" data-trigger="hover" data-content="Destination PIC NAME harus wajib diisi.." data-original-title="Informasi User" ></i></label>
                                                                <div class="controls controls-row">
                                                                <input type="text" class="input-block-level" placeholder="Enter PIC NAME" style="width:339px;position:relative;margin:0px -40px" id="pic_name_destination"  name="pic_name_destination" required>
                                                                <label style="position: absolute;margin:-20px -165px;">PIC NAME</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <hr>
                                                    </div>
                                                </div>
                                                    <div class="row-fluid">
                                                            <div class="span12">
                                                                <div class="control-group">
                                                                    <label class="control-label" style="font-family: 'Courier', monospace;font-size:16px;font: bold;"> <strong>Detail&nbsp;Orders</strong></label>
                                                                    <div class="controls">
                                                                        <hr>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <div class="span9">
                                                                <div class="control-group">
                                                                    <label class="control-label">Sub Service</label>
                                                                        <div class="controls">
                                                                            <select class="subs onchangeCustomer span12" data-placeholder="Sub Services" id="sub_servicess" style="width:320px;" name="sub_services" required>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-fluid">
                                                                    <div class="span8">
                                                                            <div class="control-group">
                                                                                <label class="control-label">Item</label>
                                                                                <div class="controls">
                                                                                    <select class="form-control itm" data-placeholder="Items" style="width:68%" id="items_tc" name="items_tc" required>
                                                                                    </select>&nbsp;<span class="add-on"><a type="button" data-placement="top" id="modal" data-toggle="modal" data-target="#add_item" data-whatever="" class="itemsadd btn btn-success"><i class="icon-plus"></i> Add Item</a></span>
                                                                                    {{-- <span class="add-on"><a type="button" id="refresh" class="btn btn-primary"><i class="fas fa-sync-alt"></i> Reload</a></span> --}}
                                                                                    <div class="row-fluid">
                                                                                        {{-- <div class="span8">
                                                                                            <br> --}}
                                                                                    {{-- @if ($errors->has('items'))
                                                                                            <span id="items_error" class="alert alert-danger span10" style="width:300px;text-align: center">{{ $errors->first('items') }}</span>
                                                                                        @endif --}}
                                                                                    {{-- </div> --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                        <div class="row-fluid">
                                                                <div class="span3">
                                                                    <div class="control-group">
                                                                        <label class="control-label">Quantity/Rate/Total</label>
                                                                        <div class="controls">
                                                                            <div class="input-prepend">
                                                                                <input class="validate[required]" type="text" value="{{ old('qty') }}" style="width:135px;" placeholder="Enter Quantity" maxlength="5" id="qty" name="qty">
                                                                                <span class="add-on">Pcs \ Unit \ Kg</i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        <div class="span3">
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <div class="input-prepend input-append">
                                                                            <span class="add-on">Rp</span><input class="validate[required] input-element" type="text" value="{{ old('rate') }}" placeholder="Enter price" maxlength="14" id="rate" name="rate" style="width;235%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="span6">
                                                                    <div class="control-group">
                                                                        <div class="controls">
                                                                            <div class="input-prepend">
                                                                                <span class="add-on">Rp</i></span><input readonly="true" data-placement="top" class="popovers" data-trigger="hover" style="width:223px" data-content="Harga akhir" data-original-title="Informasi total harga"
                                                                                        type="text" maxlength="30" id="total_rate" name="total_rate" style="width;235%" placeholder="Total Harga" >
                                                                                    </div>
                                                                    </div>
                                                              </div>
                                                        </div>
                                                        <b><span id="format"></span></b>
                                                    </div>
                                                    <div class="row-fluid">
                                                            <div class="span3">
                                                                <div class="control-group">
                                                                    <label class="control-label">ETD/ETA/Time Zone</label>
                                                                        <div class="controls">
                                                                            <div class="input-prepend">
                                                                            <span class="add-on" style="cursor: pointer" id="btnPickers_etd"><i class="icon-calendar" id="calenders_etd"></i></span>
                                                                                <input class="validate[required]" type="text" value="{{ old('etd') }}" style="width:208px;" placeholder="Enter ETD" id="etd" name="etd">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        <div class="span3">
                                                            <div class="control-group">
                                                                <div class="controls">
                                                                    <div class="input-prepend">
                                                                    <span class="add-on" style="cursor: pointer"id="btnPickers_eta"><i class="icon-calendar" id="btnPickers_eta"></i></span>
                                                                    <input class="validate[required]" type="text" value="{{ old('eta') }}" style="width:208px" placeholder="Enter ETA" id="eta" name="eta">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            <div class="span2">
                                                                <div class="control-group">
                                                                    <div class="controls">
                                                                        <select class="input-small m-wrap cusets" data-trigger="hover" style="width:223px" data-content="Data WOM" data-original-title="Information" id="time_zone" name="time_zone">
                                                                            <option value="WIB">WIB</option>
                                                                            <option value="WITA">WITA</option>
                                                                            <option value="WIT">WIT</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                                <div class="span3">
                                                                    <div class="control-group">
                                                                        <label class="control-label">Information C\V\AW\CW</label>
                                                                            <div class="controls">
                                                                                <div class="input-prepend">
                                                                                    <input type="text" class="validate[required]" value="{{ old('collie') }}" style="width:150px;" placeholder="Enter Collie" maxlength="5" id="collie" name="collie">
                                                                                    <span class="add-on">Collie</i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="span2">
                                                                            <div class="control-group">
                                                                                <div class="controls" style="margin-left:140px;margin-top: -0,5px;">
                                                                                    <div class="input-prepend">
                                                                                        <input type="text" class="validate[required]" value="{{ old('volume') }}" maxlength="5" style="width:133px" placeholder="Enter Volume" id="volume" name="volume">
                                                                                    <span class="add-on">cm³</i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <div class="span2">
                                                                    <div class="control-group">
                                                                            <div class="controls" style="margin-left:157px;margin-top: -0,5px;">
                                                                            <div class="input-prepend">
                                                                                <input type="text" class="validate[required]" value="{{ old('actual_weight') }}" maxlength="5" style="width:130px" placeholder="Enter Actual Weight" id="actual_weight" name="actual_weight">
                                                                            <span class="add-on">Kg</i></span>/
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="span2">
                                                                    <div class="control-group">
                                                                            <div class="controls" style="margin-left:165px;margin-top: -0,5px;">
                                                                            <div class="input-prepend">
                                                                            <input type="text" class="validate[required]" value="{{ old('chargeable_weight') }}" maxlength="5" style="width:170px" placeholder="Enter Chargeable Weight" id="chargeable_weight" name="chargeable_weight">
                                                                            <span class="add-on">Kg</i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                                <div class="span11">
                                                                    <div class="control-group">
                                                                        <label class="control-label">
                                                                            Notes
                                                                        </label>
                                                                        <div class="controls">
                                                                            <textarea class="span12" style="width:109%" id="notes" name="notes" rows="3"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="span6 hidden">
                                                                    <div class="control-group hidden">
                                                                        {{-- <label class="control-label" >Fetch Id Saved_origin</label> --}}
                                                                        <label class="control-label"></label>
                                                                        <div class="controls controls-row">
                                                                            <input type="hidden" class="input-block-level" placeholder="" id="test_sb_service"  name="test_sb_service">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                            <div class="row-fluid">
                                                <div class="span12" style="text-align:right;">
                                                    <div class="form-actions" style="">
                                                        <button id="addorders" type="submit" class="btn btn-success">Submit Your Order</button>
                                                        {{-- <a href="#myModal3" role="button" type="submit" class="btn btn-primary" data-toggle="modal">Confirm</a> --}}
                                                        <a class="btn btn-warning" href="{{ route('transport.static', $some) }}">Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <ul class="pager wizard">
                                                        <li class="previous first"><a href="javascript:;">First</a></li>
                                                        <li class="previous"><a href="javascript:;">Previous</a></li>
                                                        {{-- <li class="next last"><a href="javascript:;">Last</a></li> --}}
                                                        <li class="next hover"><a href="javascript:;">Next</a></li>
                                                        {{-- <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li> --}}
                                                        <li style="display:none;"><a  href="javascript:;">Finish</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php
$data=[

    'content' => $jobs_order_idx

];
@endphp
@include('admin.transport.transport_order.modal_customer', ['data' => $data])
@include('admin.transport.transport_order.modal_address_book')
@include('admin.transport.transport_order.modal_add_item_customer')
@endsection
@section('javascript')
<script src=" {{ asset('js/src-vue/vue-srcs.js')}}"></script>
<script src="{{ asset('js/src-vue/layers-form-transport.js') }}"></script>
<script src="{{ asset('js/cleave/cleaved.min.js') }}"></script>
<script src="{{ asset('js/cleave/cleaved-phone.{country}.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7/dist/sweetalert2.all.min.js"></script> --}}
<script src=" {{ asset('js/sweet-alerts/sweet-alerts.min.js')}}"></script>
@include('sweetalert::view')
@include('notify::messages')
@notifyJs
<script src="{{ asset('js/jquery-1.8.3.min.js') }}"></script>
<script src="{{ asset('js/dupselect.min.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{asset('js/form-wizard.js')}}"></script>
<script src="{{ asset('js/jquery-popup.js') }}"></script>
<script src="{{ asset('js/jquery.blockui.js') }}"></script>
<script src="{{ asset('js/jquery.sparkline.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('assets/data-tables/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/data-tables/DT_bootstrap.js') }}"></script>
<script src="{{ asset('assets/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/common-scripts.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.js" --}}
{{-- integrity="sha256-6mrxHx3BPUM9vJm3dH7jULYW56kexJcIO6LPneaBnS4=" crossorigin="anonymous"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js" --}}
{{-- integrity="sha256-CGeJcrPLTi6rwHUfGazTyGhlRmLVJ4RIX79X1FmNQ3k=" crossorigin="anonymous"></script> --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script src="{{ asset('js/form-validation-script.js') }}"></script>
<script src="{{ asset('js/input-errors-address-transport.js') }}"></script>
<script src="{{ asset('js/moduleTransport/ModuleTransportForms.js') }}"></script>
<script src="{{ asset('js/mod-validation-customer/mod-validate-field-customers.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
<script async language="javascript" type="text/javascript">

        $(function(){
            $('#addorders').click(function (e) {
                e.preventDefault();
                let customers = $("#customers_name").val();
                $("#addorders").prop( "disabled", true );
                $("#addorders").text('Please wait proccessing...');
                    return new Promise((resolve, reject) => {
                    // let awaitTransport = new Promise((resolve, reject) => {
                        setTimeout(() => resolve(StoreOrderTransport()), 3500)
                    });
            });
        });

    async function StoreOrderTransport() {
            
            const SuccessAlertsTransportAPI = Swal.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 7500
            })

            let customers = $("#customers_name").val();
            let id_project = $("#id_project").val();
                        
            // origin
            let saved_origin = $("#saved_origin").val();
            let origin = $("#origin").val();
            let origin_city = $("#origin_city").val();
            let origin_address = $("#origin_address").val();
            let pic_phone_origin = $("#pic_phone_origin").val();
            let pic_name_origin = $("#pic_name_origin").val();
            let id_origin_city = $("#id_origin_city").val();

            // destination
            let saved_destination = $("#saved_destination").val();
            let destination = $("#destination").val();
            let destination_city = $("#destination_city").val();
            let destination_address = $("#destination_address").val();
            let pic_phone_destination = $("#pic_phone_destination").val();
            let pic_name_destination = $("#pic_name_destination").val();
            let id_destination_city = $("#id_destination_city").val();

            // detail order
            let sub_servicess = $("#sub_servicess").val();
            let items_tc = $("#items_tc").val();
            let qty = $("#qty").val();
            let harga = $("#rate").val();
            let total_rate = $("#total_rate").val();
            let etd = $("#etd").val();
            let eta = $("#eta").val();
            let time_zone = $("#time_zone").val();

            let collie = $("#collie").val();
            let volume = $("#volume").val();
            let actual_weight = $("#actual_weight").val();
            let chargeable_weight = $("#chargeable_weight").val();
            let notes = $("#notes").val();

                const apiTransports = "{{ route('transport.stored.static', $some ) }}";
                const dataTransports = { 

                            token : "{{ csrf_token() }}",
                            customers: customers,
                            id_project: id_project,

                            // origin
                            saved_origin: saved_origin,
                            origin: origin,
                            origin_city: origin_city,
                            origin_address: origin_address,
                            pic_phone_origin: pic_phone_origin,
                            pic_name_origin: pic_name_origin,
                            id_origin_city: id_origin_city,

                            // destination 
                            saved_destination: saved_destination,
                            destination: destination,
                            destination_city: destination_city,
                            destination_address: destination_address,
                            pic_phone_destination: pic_phone_destination,
                            pic_name_destination: pic_name_destination,
                            id_destination_city: id_destination_city,

                            // detail order
                            sub_servicess:sub_servicess,
                            items_tc:items_tc,
                            harga:harga,
                            qty:qty,
                            total_rate:total_rate,
                            eta:eta,
                            etd:etd,
                            time_zone:time_zone,
                            
                            collie:collie,
                            volume:volume,
                            actual_weight:actual_weight,
                            chargeable_weight:chargeable_weight,
                            notes:notes
                           
                        };

        try 
            {
                // allowrd with mode cors with API
                const responseTransport = await fetch(apiTransports, {

                        method: 'GET',
                        cache: 'no-cache',
                        credentials: 'same-origin',
                        redirect: 'follow',
                        referrer: 'no-referrer',
                        body: JSON.stringify(dataTransports),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Content-Type': 'application/json'
                    }

                });

                const dataJson = await responseTransport.json();
                let TransportPromise = new Promise((resolve, reject) => {
                    setTimeout(() => resolve(dataJson), 1000)
                });
    
                    let transportPromises = await TransportPromise;

                //customer 
                $("#customers_name").empty();
                // id_project ->  customer in izzy refs['customers_name']
                $("#id_project").val('');
                
                // origin detail
                $("#saved_origin").empty();
                $("#origin").val('');
                $("#origin_city").val('');
                $("#origin_address").val('');
                $("#origin_city").empty();
                $("#pic_phone_origin").val('');
                $("#pic_name_origin").val('');
                $("#id_origin_city").val('');
    
                // destination detail
                $("#saved_destination").empty();
                $("#destination_city").empty();
                $("#destination").val('');
                $("#destination_city").val('');
                $("#destination_address").val('');
                $("#pic_phone_destination").val('');
                $("#pic_name_destination").val('');
                $("#id_destination_city").val('');

                //detail order transport
                $("#sub_servicess").empty();
                $("#items_tc").empty();
                $("#qty").val('');
                $("#total_rate").val('');
                $("#eta").val('');
                $("#etd").val('');
                $("#time_zone").val('');
                $("#rate").val('');
                
                $("#collie").val('');
                $("#volume").val('');
                $("#actual_weight").val('');
                $("#chargeable_weight").val('');
                $("#notes").val('');
                    
                $("#addorders").prop("disabled", false);
                $("#addorders").text("Submit Your Order");

                // console.log('Success:', JSON.stringify(transportPromises.order_id));
                // return redirect()->route('transport.static', session()->get('id'));
                // let leavemeplease = document.location.href = "{!! route('transport.static', session()->get('id')); !!}";

                // return leavemeplease;
                // SuccessAlertsTransportAPI.fire({
                //     type: 'success',
                //     title: transportPromises.success +''+ 
                // })
            } 
                catch (errors) {
                  
                    $.ajaxSetup(
                                    {
                                            headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                            }
                        )
                    ;

                    let request = $.ajax({
                    
                    headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: apiTransports,
                    type: "GET",
                    dataType: "json",
                    data: dataTransports,
                    success: function (data) {

                        // console.log('data-success',data)
                        // do something else without data
                            
                },
                complete:function(data){

                    // TODO: do something with complete arguments
                 
                },
                    error: function(jqXhr, json, errorThrown){
                     
                        let responses = $.parseJSON(jqXhr.responseText).errors;
                            errorsHtml = '<div class="alert alert-danger"><ul>';
                        $.each( responses, function( key, value ) {
                                errorsHtml +=  value[0] +'<br/>';
                            }
                        );
                            errorsHtml += '</ul></div>';
                            buttonconfirm = '<div class="badge badge-info closeme" style="font-size:14px;height:19px;width:40px;cursor: pointer">Okay</div>';
                            let TransportPromise = new Promise((resolve, reject) => {
                                setTimeout(() =>        
                                        Swal({
                                        title: "Code Error " + jqXhr.status + ': ' + errorThrown,
                                        text: "Maaf proses upload gagal diproses !",
                                            confirmButtonColor: '#3085d6',
                                            html: errorsHtml +'<br/>'+ buttonconfirm,
                                            width: 'auto',
                                            showConfirmButton: false,
                                            // confirmButtonText: '<div class="badge badge-success">Ok</div>',
                                            type: 'error'
                                        }).then((result) => {
                                        if (result.value) {

                                                return false;
                                    
                                    }
                            }),
                                $("#addorders").prop("disabled", false),
                                $("#addorders").text("Submit Your Order"), 1000)
                        });
                    }
                }
            );
                    // const ErrorsAlertsTransportAPI = Swal.mixin({
                    //     toast: true,
                    //     position: 'bottom-end',
                    //     showConfirmButton: false,
                    //     timer: 7000
                    // })

                    // let TransportPromiseErrors = new Promise((resolve, reject) => {
                    //     setTimeout(() => reject(console.error('Error:', errors)), 2000)
                    // });

                        // ErrorsAlertsTransportAPI.fire({
                        //     type: 'error',
                        //     title: `Data gagal disimpan `+ errors
                        // })

                }

        }

</script>
@endsection
