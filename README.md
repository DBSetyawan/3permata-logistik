# 3 PERMATA SYSTEM

Ini adalah project manajemen administrasi pada sistem order warehousing dan ekspedisi pada PT. Tiga Permata Logistik

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
PHP >= 7.1.*
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
first you can install web server nginx or equivalent

```

```
process deployments
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

php 

### Break down into end to end tests

You are free to define other testing environment configuration values as necessary. The testing environment variables may be configured in the phpunit.xml file, but make sure to clear your configuration cache using the config:clear Artisan command before running your tests!

```
PHP server development 7.1.*
```

### And coding style tests

Opening <?php tag.
File-level docblock.
One or more declare statements.
The namespace declaration of the file.
One or more class-based use import statements.
One or more function-based use import statements.
One or more constant-based use import statements.
The remainder of the code in the file.

```
VSCODE
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Laravel](https://laravel.com/docs/5.7) - The web framework used

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/tigapermata/tigapermatasystem.git). 

## Authors

* **3 Permata System** - *Initial work* - [Bitbucket](https://github.org/tigapermata)

See also the list of [contributors](https://github.com/tigapermata/tigapermatasystem.git) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* Technology Information
* Tracking Information
* Transport Technology

